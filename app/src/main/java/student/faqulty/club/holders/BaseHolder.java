package student.faqulty.club.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import student.faqulty.club.adapters.BaseRecycleAdapter;

/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseHolder extends RecyclerView.ViewHolder {

    protected Context context;
    protected BaseRecycleAdapter.RecyclerClickInterface clickListener;
    private int position;

    public BaseHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
    }

    public BaseRecycleAdapter.RecyclerClickInterface getClickListener() {
        return clickListener;
    }

    public void setClickListener(BaseRecycleAdapter.RecyclerClickInterface clickListener) {
        this.clickListener = clickListener;
    }

//    protected void sendClickEvent(int actionType) {
//        sendClickEvent(actionType, model);
//    }
//
//    protected void sendClickEvent(int actionType, Object model) {
//        if (clickListener != null) {
//            clickListener.onItemClicked(getAdapterPosition(), itemView, model, actionType);
//        }
//    }

    protected View findView(int id) {
        return itemView.findViewById(id);
    }

    protected TextView findTv(int id) {
        return (TextView) findView(id);
    }

    public void onBind(int position, Object obj) {
        this.position = position;
    }


    protected void onEventAction(int actionType, Object object) {
        if (clickListener != null) {
            clickListener.onItemClick(itemView, getAdapterPosition(), object, actionType);
        }
    }

}


package student.faqulty.club.holders.send;

import android.view.View;
import android.widget.TextView;

import student.faqulty.club.R;
import student.faqulty.club.holders.BaseHolder;
import student.faqulty.club.model.chat.ChatMessage;
import student.faqulty.club.model.chat.ChatText;
import student.faqulty.club.model.chat.MetaInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by admin on 2/22/17.
 */

public class SendTextMessageHolder extends BaseHolder {
    private TextView tvMessage,tvTimeStamp;
    public SendTextMessageHolder(View itemView) {
        super(itemView);
        tvMessage=findTv(R.id.tv_message);
        tvTimeStamp=findTv(R.id.tv_time);
    }
    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ChatMessage chatMessage= (ChatMessage) obj;
        ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
        setComanData(chatMessage,chatText.getText());
    }

    protected void setComanData(ChatMessage chatMessage, String chatText) {
        if(chatText!=null){
            tvMessage.setText(chatText);
        }
        long timeStamp = chatMessage.getMessageId();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("hh:mm a");
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        String format = simpleDateFormat.format(calendar.getTime());
        tvTimeStamp.setText(format);
    }

}

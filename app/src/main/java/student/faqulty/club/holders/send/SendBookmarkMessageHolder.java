package student.faqulty.club.holders.send;

import android.view.View;
import android.widget.TextView;

import student.faqulty.club.R;
import student.faqulty.club.holders.BaseHolder;


/**
 * Created by admin on 2/22/17.
 */

public class SendBookmarkMessageHolder extends BaseHolder {
    private TextView tvQuestionSet,tvClasses,tvTimeStamp;
    public SendBookmarkMessageHolder(View itemView) {
        super(itemView);
        tvQuestionSet=findTv(R.id.tv_question_set);
        tvClasses=findTv(R.id.tv_classes);
        tvTimeStamp=findTv(R.id.tv_time);
    }
}

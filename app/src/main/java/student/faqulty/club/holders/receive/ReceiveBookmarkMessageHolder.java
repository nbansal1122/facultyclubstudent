package student.faqulty.club.holders.receive;

import android.view.View;
import android.widget.TextView;

import student.faqulty.club.R;
import student.faqulty.club.holders.send.SendBookmarkMessageHolder;
import student.faqulty.club.model.chat.ChatMessage;

/**
 * Created by admin on 2/22/17.
 */

public class ReceiveBookmarkMessageHolder extends SendBookmarkMessageHolder {
    private TextView tvUser;
    public ReceiveBookmarkMessageHolder(View itemView) {
        super(itemView);
        tvUser=findTv(R.id.tv_username);
    }
    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ChatMessage chatMessage= (ChatMessage) obj;
        tvUser.setText(chatMessage.getFromUserName());
    }

}

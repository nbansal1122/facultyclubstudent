package student.faqulty.club.holders.receive;

import android.view.View;
import android.widget.TextView;

import student.faqulty.club.R;
import student.faqulty.club.holders.send.SendMediaMessageHolder;
import student.faqulty.club.model.chat.ChatMessage;

/**
 * Created by admin on 2/22/17.
 */

public class ReceiveMediaMessageHolder extends SendMediaMessageHolder {
    private TextView tvUserName;
    public ReceiveMediaMessageHolder(View itemView) {
        super(itemView);
        tvUserName=findTv(R.id.tv_username);
    }
    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        ChatMessage chatMessage= (ChatMessage) obj;
        tvUserName.setText(chatMessage.getFromUserName());

    }

}

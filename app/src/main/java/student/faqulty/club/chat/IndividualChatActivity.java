package student.faqulty.club.chat;

import android.os.Bundle;

import java.io.Serializable;

import student.faqulty.club.chatutil.SocketUtil;
import student.faqulty.club.model.BaseApi;
import student.faqulty.club.model.UploadImageResponse;
import student.faqulty.club.model.chat.ChatMessage;
import student.faqulty.club.model.chat.MetaInfo;
import student.faqulty.club.model.group.GroupData;
import student.faqulty.club.model.tutor.TutorResponse;
import student.faqulty.club.util.AppController;
import student.faqulty.club.R;
import student.faqulty.club.chatutil.SocketUtil;
import student.faqulty.club.model.UploadImageResponse;
import student.faqulty.club.model.chat.ChatMessage;
import student.faqulty.club.model.chat.MetaInfo;
import student.faqulty.club.model.tutor.TutorResponse;
import student.faqulty.club.util.AppController;

import simplifii.framework.utility.AppConstants;

public class IndividualChatActivity extends BaseChatActivity {

    private TutorResponse tutorResponse;
    private GroupData groupData;

    @Override
    protected void loadBundle(Bundle bundle) {
        BaseApi serializable = (BaseApi) bundle.getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        if(serializable instanceof TutorResponse){
            tutorResponse = (TutorResponse) serializable;
        }
        if(serializable instanceof GroupData){
            groupData = (GroupData) serializable;
        }
    }

    @Override
    protected void refresh() {
        if(tutorResponse!=null){
            initToolBar(tutorResponse.getName());
            initLoader(tutorResponse.getId());
            AppController.chatUserId=tutorResponse.getId();
        }
        if(groupData!=null){
            initToolBar(groupData.getName());
            initLoader(groupData.getId());
            AppController.chatUserId=groupData.getId();
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.fc_icon;
    }

    @Override
    protected void onHomePressed() {

    }

    @Override
    protected void sendTextMessage(String message) {
        super.sendTextMessage(message);
        if (tutorResponse != null) {
            String metaInfoStrign = MetaInfo.getMetaInfoString(message);
            ChatMessage chatMessage = ChatMessage.sendIndividualTextToStudent(metaInfoStrign, tutorResponse.getId());
            SocketUtil.sendChatMessage(chatMessage, this);
        }
    }

    @Override
    protected void sendFile(UploadImageResponse uploadImageResponse, String mediaType) {
        if (tutorResponse != null) {
            String metaInfoStrign = MetaInfo.getMetaInfoFile(uploadImageResponse.getData());
            ChatMessage chatMessage = ChatMessage.sendIndividualFileToStudent(metaInfoStrign, tutorResponse.getId());
            SocketUtil.sendChatMessage(chatMessage, this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.chatUserId=0;
    }
}

package student.faqulty.club.chat;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.activeandroid.content.ContentProvider;
import com.activeandroid.query.Select;
import student.faqulty.club.Fragments.AddImageFragment;
import student.faqulty.club.Fragments.MediaFragment;
import student.faqulty.club.R;
import student.faqulty.club.adapters.ChatRecycleAdapter;
import student.faqulty.club.model.Image;
import student.faqulty.club.model.UploadImageResponse;
import student.faqulty.club.model.chat.ChatMessage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

/**
 * Created by admin on 2/22/17.
 */

public class BaseChatActivity extends BaseActivity implements AttachmentFragment.OnAttachmentSelectListener, View.OnFocusChangeListener {
    private AttachmentFragment attachmentFragment;
    private MediaFragment mediaFragment = new MediaFragment();
    private LinearLayout layAttachment;
    private boolean isAttachmentOpen;
    private EditText etMessage;
    private List<ChatMessage> chatMessages = new ArrayList<>();
    private ChatRecycleAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayout progressFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initViews();
        refresh();
    }

    protected void refresh() {

    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_chat);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ChatRecycleAdapter(this, chatMessages);
        recyclerView.setAdapter(adapter);
        attachmentFragment = AttachmentFragment.getInstance(this);

        etMessage = (EditText) findViewById(R.id.et_message);
        etMessage.setOnFocusChangeListener(this);
        layAttachment = (LinearLayout) findViewById(R.id.lay_attachment_fragment_container);
        progressFrame = (LinearLayout) findViewById(R.id.frame_progress);
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "Media Fragment").commit();
        setOnClickListener(R.id.iv_send, R.id.iv_attachment, R.id.iv_camera, R.id.lay_attachment_container);
    }

    protected void initLoader(final long userId) {
//        refreshBaseList(userId);
        getSupportLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int arg0, Bundle cursor) {
                return new CursorLoader(BaseChatActivity.this,
                        ContentProvider.createUri(ChatMessage.class, null),
        null, "toUserID=? or fromUserID=?", new String[]{userId+"", userId+""}, null
                );
            }

            @Override
            public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
                if (cursor != null) {
                    chatMessages.clear();
                    while (cursor.moveToNext()) {
                        ChatMessage chatMessage = new ChatMessage();
                        chatMessage.loadFromCursor(cursor);
                        chatMessages.add(chatMessage);
                    }
                }
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(adapter.getItemCount() - 1);
            }

            @Override
            public void onLoaderReset(Loader<Cursor> arg0) {
                chatMessages.clear();
                adapter.notifyDataSetChanged();
            }
        });
    }

    protected void refreshBaseList(long userId) {
        List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("toUserID=? or fromUserID=?", userId, userId).execute();
        this.chatMessages.clear();
        this.chatMessages.addAll(chatMessages);
        adapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_send:
                String message = etMessage.getText().toString().trim();
                if (!TextUtils.isEmpty(message)) {
                    sendTextMessage(message);
                    etMessage.setText("");
                }
                break;
            case R.id.iv_attachment:
                showAttachment();
                break;
            case R.id.iv_camera:
                getImage();
                break;
            case R.id.lay_attachment_container:
                closeAttachment();
                break;
        }
    }

    protected void sendTextMessage(String message) {
    }

    private void showAttachment() {
        if (isAttachmentOpen) {
            closeAttachment();
            return;
        }
        isAttachmentOpen = true;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.lay_attachment_fragment_container, attachmentFragment).commit();
        showSlideUpAnimation();
    }

    private void showSlideUpAnimation() {
        showVisibility(R.id.lay_attachment_container);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in_top);
        layAttachment.setAnimation(animation);
    }

    private void closeAttachment() {
        isAttachmentOpen = false;
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        layAttachment.clearAnimation();
        layAttachment.setAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                hideVisibility(R.id.lay_attachment_container);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void getImage() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.share_image));
        mediaFragment.setImageListener(new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                if (bitmap != null) {
                    uploadImage(bitmap);
                }
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                if (images.size() > 0) {
                    String path = images.get(0).getFilePath();
                }
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(mediaFragment);
        bottomSheetDialogFragment.setArguments(bundle);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    private void uploadImage(Bitmap bitmap) {
        showVisibility();
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        try {
            File file = Util.getFile(bitmap, "pamphlet logo");
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    sendFile(uploadImageResponse, AppConstants.MEDIA_TYPES.IMAGE);
                }
                break;
        }
    }

    protected void sendFile(UploadImageResponse uploadImageResponse, String image) {

    }

    @Override
    public void showProgressDialog() {
        progressFrame.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressFrame.setVisibility(View.GONE);
    }

    @Override
    public void onAttachmentSelect(int attachmentType) {
        closeAttachment();
    }

    @Override
    public void onBackPressed() {
        if(isAttachmentOpen){
            closeAttachment();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }
}

package student.faqulty.club.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileCompleteness {

    @SerializedName("personalDetail")
    @Expose
    private Integer personalDetail;
    @SerializedName("location")
    @Expose
    private Integer location;
    @SerializedName("tuition")
    @Expose
    private Integer tuition;
    @SerializedName("qualification")
    @Expose
    private Integer qualification;
    @SerializedName("social")
    @Expose
    private Integer social;
    @SerializedName("content")
    @Expose
    private Integer content;
    @SerializedName("review")
    @Expose
    private Integer review;

    /**
     * @return The personalDetail
     */
    public Integer getPersonalDetail() {
        return personalDetail;
    }

    /**
     * @param personalDetail The personalDetail
     */
    public void setPersonalDetail(Integer personalDetail) {
        this.personalDetail = personalDetail;
    }

    /**
     * @return The location
     */
    public Integer getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Integer location) {
        this.location = location;
    }

    /**
     * @return The tuition
     */
    public Integer getTuition() {
        return tuition;
    }

    /**
     * @param tuition The tuition
     */
    public void setTuition(Integer tuition) {
        this.tuition = tuition;
    }

    /**
     * @return The qualification
     */
    public Integer getQualification() {
        return qualification;
    }

    /**
     * @param qualification The qualification
     */
    public void setQualification(Integer qualification) {
        this.qualification = qualification;
    }

    /**
     * @return The social
     */
    public Integer getSocial() {
        return social;
    }

    /**
     * @param social The social
     */
    public void setSocial(Integer social) {
        this.social = social;
    }

    /**
     * @return The content
     */
    public Integer getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    public void setContent(Integer content) {
        this.content = content;
    }

    /**
     * @return The review
     */
    public Integer getReview() {
        return review;
    }

    /**
     * @param review The review
     */
    public void setReview(Integer review) {
        this.review = review;
    }

}


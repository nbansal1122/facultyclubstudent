package student.faqulty.club.model.notifications;

/**
 * Created by raghu on 25/1/17.
 */
public class Notification {
    private int id ;
    private String type ;
    private String message ;
    private String createdOn ;
    private int userId ;
    private int seen ;

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public int getUserId() {
        return userId;
    }

    public int getSeen() {
        return seen;
    }
}

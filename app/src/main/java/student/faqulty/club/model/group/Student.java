package student.faqulty.club.model.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student {

@SerializedName("id")
@Expose
private int id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("email")
@Expose
private String email;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }
}
package student.faqulty.club.model.tutor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TutorLocation implements Serializable{

@SerializedName("id")
@Expose
private int id;
@SerializedName("tutorId")
@Expose
private int tutorId;
@SerializedName("cityId")
@Expose
private int cityId;
@SerializedName("localityId")
@Expose
private int localityId;
@SerializedName("primary")
@Expose
private int primary;
@SerializedName("createdOn")
@Expose
private String createdOn;
//@SerializedName("city")
//@Expose
//private City city;
//@SerializedName("locality")
//@Expose
//private Locality locality;

    public int getId() {
        return id;
    }

    public int getTutorId() {
        return tutorId;
    }

    public int getCityId() {
        return cityId;
    }

    public int getLocalityId() {
        return localityId;
    }

    public int getPrimary() {
        return primary;
    }

    public String getCreatedOn() {
        return createdOn;
    }
//
//    public City getCity() {
//        return city;
//    }
//
//    public Locality getLocality() {
//        return locality;
//    }
}
package student.faqulty.club.model.tutor;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class City  implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}


package student.faqulty.club.model.group;

import student.faqulty.club.model.BaseApi;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupResponse extends BaseApi{

@SerializedName("data")
@Expose
private List<GroupData> data;

public List<GroupData> getData() {
return data;
}

public void setData(List<GroupData> data) {
this.data = data;
}

}

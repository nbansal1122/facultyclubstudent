package student.faqulty.club.model.push_notification;

import java.io.Serializable;

/**
 * Created by admin on 2/28/17.
 */

public class NotificationData implements Serializable{
    private int type;
    private String title;
    private String message;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

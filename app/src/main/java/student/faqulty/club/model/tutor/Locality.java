package student.faqulty.club.model.tutor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Locality implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("cityId")
    @Expose
    private int cityId;
    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    public int getId() {
        return id;
    }

    public int getCityId() {
        return cityId;
    }

    public String getLocality() {
        return locality;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}

package student.faqulty.club.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by Dhillon on 04-11-2016.
 */
public class UserDetailsApi {

    @SerializedName("oneOnOneTutionCharge")
    @Expose
    private Object oneOnOneTutionCharge;
    @SerializedName("groupTutionCharge")
    @Expose
    private Object groupTutionCharge;
    @SerializedName("weekdayTutionTime")
    @Expose
    private Object weekdayTutionTime;
    @SerializedName("weekendTutionTime")
    @Expose
    private Object weekendTutionTime;
    @SerializedName("coursesTaught")
    @Expose
    private List<Object> coursesTaught = new ArrayList<Object>();
    @SerializedName("tutorBoard")
    @Expose
    private List<Object> tutorBoard = new ArrayList<Object>();
    @SerializedName("tutorTeachingMode")
    @Expose
    private List<Object> tutorTeachingMode = new ArrayList<Object>();
    @SerializedName("tutorTeachingLanguage")
    @Expose
    private List<Object> tutorTeachingLanguage = new ArrayList<Object>();
    @SerializedName("tutorSchool")
    @Expose
    private List<Object> tutorSchool = new ArrayList<Object>();
    @SerializedName("tutorLocation")
    @Expose
    private List<Object> tutorLocation = new ArrayList<Object>();
    @SerializedName("tutorAwards")
    @Expose
    private List<Object> tutorAwards = new ArrayList<Object>();
    @SerializedName("likeCount")
    @Expose
    private Integer likeCount;
    @SerializedName("reviewCount")
    @Expose
    private Integer reviewCount;
    @SerializedName("tutionCenterParent")
    @Expose
    private Object tutionCenterParent;
    @SerializedName("rawProfileCompleteness")
    @Expose
    private String rawProfileCompleteness;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("profileCompleteness")
    @Expose
    private ProfileCompleteness profileCompleteness;
    @SerializedName("facultyProfileURI")
    @Expose
    private Object facultyProfileURI;
    @SerializedName("userTypeString")
    @Expose
    private String userTypeString;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userType")
    @Expose
    private Integer userType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("location")
    @Expose
    private Object location;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("userDetails")
    @Expose
    private Object userDetails;
    @SerializedName("tagline")
    @Expose
    private String tagline;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("experience")
    @Expose
    private Integer experience;
    @SerializedName("emailVerified")
    @Expose
    private Integer emailVerified;
    @SerializedName("phoneVerified")
    @Expose
    private Integer phoneVerified;
    @SerializedName("lat")
    @Expose
    private Integer lat;
    @SerializedName("lon")
    @Expose
    private Integer lon;
    @SerializedName("tutorSearchScore")
    @Expose
    private Integer tutorSearchScore;
    @SerializedName("lastLogged")
    @Expose
    private String lastLogged;
    @SerializedName("fblink")
    @Expose
    private String fblink;
    @SerializedName("lnlink")
    @Expose
    private String lnlink;
    @SerializedName("twitterlink")
    @Expose
    private String twitterlink;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("registrationPhase")
    @Expose
    private String registrationPhase;
    @SerializedName("affilateId")
    @Expose
    private Integer affilateId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("testimonal")
    @Expose
    private Integer testimonal;
    @SerializedName("emailsent")
    @Expose
    private Integer emailsent;
    @SerializedName("googleToken")
    @Expose
    private Object googleToken;
    @SerializedName("facebookToken")
    @Expose
    private Object facebookToken;
    @SerializedName("tution_center_parent")
    @Expose
    private Object tution_center_parent;
    @SerializedName("tutorEducationDetail")
    @Expose
    private List<Object> tutorEducationDetail = new ArrayList<Object>();

    /**
     * @return The oneOnOneTutionCharge
     */
    public Object getOneOnOneTutionCharge() {
        return oneOnOneTutionCharge;
    }

    /**
     * @param oneOnOneTutionCharge The oneOnOneTutionCharge
     */
    public void setOneOnOneTutionCharge(Object oneOnOneTutionCharge) {
        this.oneOnOneTutionCharge = oneOnOneTutionCharge;
    }

    /**
     * @return The groupTutionCharge
     */
    public Object getGroupTutionCharge() {
        return groupTutionCharge;
    }

    /**
     * @param groupTutionCharge The groupTutionCharge
     */
    public void setGroupTutionCharge(Object groupTutionCharge) {
        this.groupTutionCharge = groupTutionCharge;
    }

    /**
     * @return The weekdayTutionTime
     */
    public Object getWeekdayTutionTime() {
        return weekdayTutionTime;
    }

    /**
     * @param weekdayTutionTime The weekdayTutionTime
     */
    public void setWeekdayTutionTime(Object weekdayTutionTime) {
        this.weekdayTutionTime = weekdayTutionTime;
    }

    /**
     * @return The weekendTutionTime
     */
    public Object getWeekendTutionTime() {
        return weekendTutionTime;
    }

    /**
     * @param weekendTutionTime The weekendTutionTime
     */
    public void setWeekendTutionTime(Object weekendTutionTime) {
        this.weekendTutionTime = weekendTutionTime;
    }

    /**
     * @return The coursesTaught
     */
    public List<Object> getCoursesTaught() {
        return coursesTaught;
    }

    /**
     * @param coursesTaught The coursesTaught
     */
    public void setCoursesTaught(List<Object> coursesTaught) {
        this.coursesTaught = coursesTaught;
    }

    /**
     * @return The tutorBoard
     */
    public List<Object> getTutorBoard() {
        return tutorBoard;
    }

    /**
     * @param tutorBoard The tutorBoard
     */
    public void setTutorBoard(List<Object> tutorBoard) {
        this.tutorBoard = tutorBoard;
    }

    /**
     * @return The tutorTeachingMode
     */
    public List<Object> getTutorTeachingMode() {
        return tutorTeachingMode;
    }

    /**
     * @param tutorTeachingMode The tutorTeachingMode
     */
    public void setTutorTeachingMode(List<Object> tutorTeachingMode) {
        this.tutorTeachingMode = tutorTeachingMode;
    }

    /**
     * @return The tutorTeachingLanguage
     */
    public List<Object> getTutorTeachingLanguage() {
        return tutorTeachingLanguage;
    }

    /**
     * @param tutorTeachingLanguage The tutorTeachingLanguage
     */
    public void setTutorTeachingLanguage(List<Object> tutorTeachingLanguage) {
        this.tutorTeachingLanguage = tutorTeachingLanguage;
    }

    /**
     * @return The tutorSchool
     */
    public List<Object> getTutorSchool() {
        return tutorSchool;
    }

    /**
     * @param tutorSchool The tutorSchool
     */
    public void setTutorSchool(List<Object> tutorSchool) {
        this.tutorSchool = tutorSchool;
    }

    /**
     * @return The tutorLocation
     */
    public List<Object> getTutorLocation() {
        return tutorLocation;
    }

    /**
     * @param tutorLocation The tutorLocation
     */
    public void setTutorLocation(List<Object> tutorLocation) {
        this.tutorLocation = tutorLocation;
    }

    /**
     * @return The tutorAwards
     */
    public List<Object> getTutorAwards() {
        return tutorAwards;
    }

    /**
     * @param tutorAwards The tutorAwards
     */
    public void setTutorAwards(List<Object> tutorAwards) {
        this.tutorAwards = tutorAwards;
    }

    /**
     * @return The likeCount
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * @param likeCount The likeCount
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * @return The reviewCount
     */
    public Integer getReviewCount() {
        return reviewCount;
    }

    /**
     * @param reviewCount The reviewCount
     */
    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    /**
     * @return The tutionCenterParent
     */
    public Object getTutionCenterParent() {
        return tutionCenterParent;
    }

    /**
     * @param tutionCenterParent The tutionCenterParent
     */
    public void setTutionCenterParent(Object tutionCenterParent) {
        this.tutionCenterParent = tutionCenterParent;
    }

    /**
     * @return The rawProfileCompleteness
     */
    public String getRawProfileCompleteness() {
        return rawProfileCompleteness;
    }

    /**
     * @param rawProfileCompleteness The rawProfileCompleteness
     */
    public void setRawProfileCompleteness(String rawProfileCompleteness) {
        this.rawProfileCompleteness = rawProfileCompleteness;
    }

    /**
     * @return The dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth The dateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return The profileCompleteness
     */
    public ProfileCompleteness getProfileCompleteness() {
        return profileCompleteness;
    }

    /**
     * @param profileCompleteness The profileCompleteness
     */
    public void setProfileCompleteness(ProfileCompleteness profileCompleteness) {
        this.profileCompleteness = profileCompleteness;
    }

    /**
     * @return The facultyProfileURI
     */
    public Object getFacultyProfileURI() {
        return facultyProfileURI;
    }

    /**
     * @param facultyProfileURI The facultyProfileURI
     */
    public void setFacultyProfileURI(Object facultyProfileURI) {
        this.facultyProfileURI = facultyProfileURI;
    }

    /**
     * @return The userTypeString
     */
    public String getUserTypeString() {
        return userTypeString;
    }

    /**
     * @param userTypeString The userTypeString
     */
    public void setUserTypeString(String userTypeString) {
        this.userTypeString = userTypeString;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The userType
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * @param userType The userType
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The gender
     */
    public Object getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(Object gender) {
        this.gender = gender;
    }

    /**
     * @return The location
     */
    public Object getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Object location) {
        this.location = location;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The userDetails
     */
    public Object getUserDetails() {
        return userDetails;
    }

    /**
     * @param userDetails The userDetails
     */
    public void setUserDetails(Object userDetails) {
        this.userDetails = userDetails;
    }

    /**
     * @return The tagline
     */
    public String getTagline() {
        return tagline;
    }

    /**
     * @param tagline The tagline
     */
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The experience
     */
    public Integer getExperience() {
        return experience;
    }

    /**
     * @param experience The experience
     */
    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    /**
     * @return The emailVerified
     */
    public Integer getEmailVerified() {
        return emailVerified;
    }

    /**
     * @param emailVerified The emailVerified
     */
    public void setEmailVerified(Integer emailVerified) {
        this.emailVerified = emailVerified;
    }

    /**
     * @return The phoneVerified
     */
    public Integer getPhoneVerified() {
        return phoneVerified;
    }

    /**
     * @param phoneVerified The phoneVerified
     */
    public void setPhoneVerified(Integer phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    /**
     * @return The lat
     */
    public Integer getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Integer lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public Integer getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    public void setLon(Integer lon) {
        this.lon = lon;
    }

    /**
     * @return The tutorSearchScore
     */
    public Integer getTutorSearchScore() {
        return tutorSearchScore;
    }

    /**
     * @param tutorSearchScore The tutorSearchScore
     */
    public void setTutorSearchScore(Integer tutorSearchScore) {
        this.tutorSearchScore = tutorSearchScore;
    }

    /**
     * @return The lastLogged
     */
    public String getLastLogged() {
        return lastLogged;
    }

    /**
     * @param lastLogged The lastLogged
     */
    public void setLastLogged(String lastLogged) {
        this.lastLogged = lastLogged;
    }

    /**
     * @return The fblink
     */
    public String getFblink() {
        return fblink;
    }

    /**
     * @param fblink The fblink
     */
    public void setFblink(String fblink) {
        this.fblink = fblink;
    }

    /**
     * @return The lnlink
     */
    public String getLnlink() {
        return lnlink;
    }

    /**
     * @param lnlink The lnlink
     */
    public void setLnlink(String lnlink) {
        this.lnlink = lnlink;
    }

    /**
     * @return The twitterlink
     */
    public String getTwitterlink() {
        return twitterlink;
    }

    /**
     * @param twitterlink The twitterlink
     */
    public void setTwitterlink(String twitterlink) {
        this.twitterlink = twitterlink;
    }

    /**
     * @return The active
     */
    public Integer getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * @return The registrationPhase
     */
    public String getRegistrationPhase() {
        return registrationPhase;
    }

    /**
     * @param registrationPhase The registrationPhase
     */
    public void setRegistrationPhase(String registrationPhase) {
        this.registrationPhase = registrationPhase;
    }

    /**
     * @return The affilateId
     */
    public Integer getAffilateId() {
        return affilateId;
    }

    /**
     * @param affilateId The affilateId
     */
    public void setAffilateId(Integer affilateId) {
        this.affilateId = affilateId;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The createdOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The testimonal
     */
    public Integer getTestimonal() {
        return testimonal;
    }

    /**
     * @param testimonal The testimonal
     */
    public void setTestimonal(Integer testimonal) {
        this.testimonal = testimonal;
    }

    /**
     * @return The emailsent
     */
    public Integer getEmailsent() {
        return emailsent;
    }

    /**
     * @param emailsent The emailsent
     */
    public void setEmailsent(Integer emailsent) {
        this.emailsent = emailsent;
    }

    /**
     * @return The googleToken
     */
    public Object getGoogleToken() {
        return googleToken;
    }

    /**
     * @param googleToken The googleToken
     */
    public void setGoogleToken(Object googleToken) {
        this.googleToken = googleToken;
    }

    /**
     * @return The facebookToken
     */
    public Object getFacebookToken() {
        return facebookToken;
    }

    /**
     * @param facebookToken The facebookToken
     */
    public void setFacebookToken(Object facebookToken) {
        this.facebookToken = facebookToken;
    }

    /**
     * @return The tution_center_parent
     */
    public Object getTution_center_parent() {
        return tution_center_parent;
    }

    /**
     * @param tution_center_parent The tution_center_parent
     */
    public void setTution_center_parent(Object tution_center_parent) {
        this.tution_center_parent = tution_center_parent;
    }

    /**
     * @return The tutorEducationDetail
     */
    public List<Object> getTutorEducationDetail() {
        return tutorEducationDetail;
    }

    /**
     * @param tutorEducationDetail The tutorEducationDetail
     */
    public void setTutorEducationDetail(List<Object> tutorEducationDetail) {
        this.tutorEducationDetail = tutorEducationDetail;
    }

    public String getCompleteImageUrl() {
        return AppConstants.PAGE_URL.PHOTO_URL + image;
    }

    public static UserDetailsApi parseJson(String json) {
        UserDetailsApi userDetail = (UserDetailsApi) JsonUtil.parseJson(json, UserDetailsApi.class);
        if (userDetail != null) {
            Preferences.saveData(Preferences.KEY_USER, json);
        }
        return userDetail;
    }

    public static UserDetailsApi getInstance() {
        UserDetailsApi user = null;
        String userJson = Preferences.getData(Preferences.KEY_USER, "");
        if (userJson != null) {
            user = parseJson(userJson);
        }
        return user;
    }

    public String getFormatedDateOfBirth() {
        return Util.convertUTCDateFormat(dateOfBirth, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, "dd-MM-yyyy");
    }
}

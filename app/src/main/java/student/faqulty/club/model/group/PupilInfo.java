package student.faqulty.club.model.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PupilInfo {

@SerializedName("id")
@Expose
private int id;
@SerializedName("tutorId")
@Expose
private int tutorId;
@SerializedName("tuitionType")
@Expose
private String tuitionType;
@SerializedName("billingCycle")
@Expose
private String billingCycle;
@SerializedName("fee")
@Expose
private int fee;
@SerializedName("classDurationHours")
@Expose
private int classDurationHours;
@SerializedName("classDurationMins")
@Expose
private int classDurationMins;
@SerializedName("createdOn")
@Expose
private String createdOn;

    public int getId() {
        return id;
    }

    public int getTutorId() {
        return tutorId;
    }

    public String getTuitionType() {
        return tuitionType;
    }

    public String getBillingCycle() {
        return billingCycle;
    }

    public int getFee() {
        return fee;
    }

    public int getClassDurationHours() {
        return classDurationHours;
    }

    public int getClassDurationMins() {
        return classDurationMins;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}

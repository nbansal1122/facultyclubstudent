package student.faqulty.club.model;

import java.io.Serializable;

public class BaseApi implements Serializable{
    String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
package student.faqulty.club.model;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by saurabh on 17-09-2016.
 */
public class LoginResponse extends BaseApi{
    String token;
    long userId;

    public String getToken() {
        return token;
    }

    public long getUserId() {
        return userId;
    }

    public static void saveUserData(LoginResponse loginResponse) {
        Preferences.saveData(AppConstants.PREF_KEYS.USER_TOKEN, loginResponse.getToken());
        Preferences.saveData(AppConstants.PREF_KEYS.USER_ID, loginResponse.getUserId());
        Preferences.saveData(Preferences.LOGIN_KEY, true);
    }
}

package student.faqulty.club.model.chat;

import student.faqulty.club.model.UploadImageData;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by admin on 2/24/17.
 */

public class MetaInfo {


    private ChatText TEXT;

    public ChatText getTEXT() {
        return TEXT;
    }

    public void setTEXT(ChatText TEXT) {
        this.TEXT = TEXT;
    }


    public static String getMetaInfoString(String message) {
        JSONObject metaJson = new JSONObject();
        try {
            metaJson.put("text", message);
        } catch (Exception e) {

        }
        return metaJson.toString();
    }

    public static ChatText getChatText(String s) {
        return new Gson().fromJson(s, ChatText.class);
    }

    public static String getMetaInfoFile(UploadImageData uploadImageData) {
        String mediaJson = new Gson().toJson(uploadImageData);
        return mediaJson;
    }

    public static UploadImageData getFile(String metaInfoString) {
        return new Gson().fromJson(metaInfoString, UploadImageData.class);
    }
}

package student.faqulty.club.model.group;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.utility.Util;
import student.faqulty.club.model.BaseApi;

public class GroupData extends BaseApi{

@SerializedName("id")
@Expose
private int id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("tutorId")
@Expose
private int tutorId;
@SerializedName("pupilInfoId")
@Expose
private int pupilInfoId;
@SerializedName("createdOn")
@Expose
private String createdOn;
@SerializedName("pupilInfo")
@Expose
private PupilInfo pupilInfo;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getTutorId() {
        return tutorId;
    }

    public int getPupilInfoId() {
        return pupilInfoId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public PupilInfo getPupilInfo() {
        return pupilInfo;
    }

    public String getFormatedCreatedDate() {
        return Util.convertDateFormat(createdOn, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, "MMMM dd");
    }

}

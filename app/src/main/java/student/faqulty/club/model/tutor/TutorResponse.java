package student.faqulty.club.model.tutor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import student.faqulty.club.model.BaseApi;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.utility.Util;

public class TutorResponse extends BaseApi implements Serializable{

    @SerializedName("tutorLocation")
    @Expose
    private List<TutorLocation> tutorLocation;
//    @SerializedName("tutorAwards")
//    @Expose
//    private List<Object> tutorAwards = null;
    @SerializedName("facultyProfileURI")
    @Expose
    private String facultyProfileURI;
    @SerializedName("userTypeString")
    @Expose
    private String userTypeString;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tagline")
    @Expose
    private String tagline;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("userType")
    @Expose
    private int userType;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    public List<TutorLocation> getTutorLocation() {
        return tutorLocation;
    }


    public String getFacultyProfileURI() {
        return facultyProfileURI;
    }

    public String getUserTypeString() {
        return userTypeString;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTagline() {
        return tagline;
    }

    public String getImage() {
        return image;
    }

    public int getUserType() {
        return userType;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public static List<TutorResponse> parseJson(String json) {
        List<TutorResponse> tutorResponses = new ArrayList<>();
        Gson gson = new Gson();
        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int x = 0; x < jsonArray.length(); x++) {
                JSONObject jsonObject = jsonArray.getJSONObject(x);
                TutorResponse tutorResponse = gson.fromJson(jsonObject.toString(), TutorResponse.class);
                tutorResponses.add(tutorResponse);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tutorResponses;
    }

    public String getFormatedCreatedDate() {
        return Util.convertDateFormat(createdOn, Util.ATTENDANCE_SERVER_CREATEDON_FORMAT, "MMMM dd");
    }
}

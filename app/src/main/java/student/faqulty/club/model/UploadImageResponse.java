package student.faqulty.club.model;

/**
 * Created by saurabh on 23-09-2016.
 */
public class UploadImageResponse extends BaseApi {
    private UploadImageData data;

    public UploadImageData getData() {
        return data;
    }

    public void setData(UploadImageData data) {
        this.data = data;
    }
}


//{
//        "id": 7,
//        "originalName": "account-selected@1.5x.png",
//        "encoding": "7bit",
//        "mimetype": "image/png",
//        "filename": "6f3fad12dc2b2e353c1c346660b1ea39",
//        "size": 1926,
//        "path": "/2016/9/23/8/6f3fad12dc2b2e353c1c346660b1ea39",
//        "uri": "/egest?file=6f3fad12dc2b2e353c1c346660b1ea39"
//        }
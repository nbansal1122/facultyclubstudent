package student.faqulty.club.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.TextView;


import student.faqulty.club.R;

import simplifii.framework.utility.AppConstants;

/**
 * Created by neeraj on 2/2/17.
 */

public class AddImageFragment extends BottomSheetDialogFragment {
    private TextView btn_camera_upld;
    private TextView btn_image_upld, tvTitle;
    private MediaFragment mediaFragment;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };
    private String titleFrag;


    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_add_image, null);
        dialog.setContentView(contentView);

        tvTitle = (TextView) dialog.findViewById(R.id.tv_title);
        btn_image_upld = (TextView) dialog.findViewById(R.id.btn_image_upload);
        btn_camera_upld = (TextView) dialog.findViewById(R.id.btn_camera_upload);

        Bundle ifPost = getArguments();
        if (ifPost != null){
            final boolean ifFromPost = ifPost.getBoolean(AppConstants.BUNDLE_KEYS.POST_FEED);
            titleFrag = ifPost.getString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE);

           tvTitle.setText(titleFrag);

            btn_image_upld.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ifFromPost){
                        mediaFragment.getImageFromGallery(mediaFragment.imageListener);
                    } else {
                        mediaFragment.getMultipleImageFromGallery(mediaFragment.imageListener);

                    }
                    dismiss();
                }

            });

        }


        btn_camera_upld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaFragment.getImageFromCamera(mediaFragment.imageListener);
                dismiss();

            }
        });
    }

    public static AddImageFragment getInctance(MediaFragment mediaFragment) {
        AddImageFragment addImageFragment=new AddImageFragment();
        addImageFragment.mediaFragment=mediaFragment;
        return addImageFragment;
    }
}

package student.faqulty.club.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import student.faqulty.club.model.Image;
import student.faqulty.club.util.FileUtil;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;

import simplifii.framework.utility.Util;

import static android.app.Activity.RESULT_OK;

public class MediaFragment extends Fragment {
    int IMAGE = 0;
    int AUDIO = 1;
    int VIDEO = 2;
    int PDF = 3;

    public final int REQUEST_CODE_GALLARY = 50;
    public final int REQUEST_CODE_CAMERA = 51;
    public final int REQUEST_CODE_AUDIO = 52;
    public final int REQUEST_CODE_PICK_VIDEO = 53;
    public final int REQUEST_CODE_GET_PDF = 54;
    public Uri imageUri;
    MediaListener mediaListener;
    ImageListener imageListener;
    public Activity activity;
    private boolean fromPostFeed;
    private String fragmentTitle;

    public static MediaFragment init(FragmentManager fragmentManager){
        MediaFragment mediaFragment=new MediaFragment();
        fragmentManager.beginTransaction().add(mediaFragment,"Media Fragment").commit();
        return mediaFragment;
    }

    private Uri cameraUri;

    public void setContextOfActivity(Activity activity) {
        this.activity = activity;
    }

    private void checkPerMission(PermissionListener permissionListener, String... permissions) {
        if (null != getActivity()) {
            activity = getActivity();
        }
        new TedPermission(activity)
                .setPermissions(permissions)
                .setPermissionListener(permissionListener).check();
    }

    public void getImageFromCamera(ImageListener imageListener) {
        this.imageListener = imageListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Folder/";
                File newdir = new File(dir);
                newdir.mkdirs();
                String file = dir + DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString() + ".jpg";


                File newfile = new File(file);
                try {
                    newfile.createNewFile();
                } catch (IOException e) {
                }

                cameraUri = Uri.fromFile(newfile);

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> arrayList) {

            }
        }, Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void getImageFromGallery(ImageListener imageListener) {
        this.imageListener = imageListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLARY);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> arrayList) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void getAudioFromPlayer(MediaListener mediaListener) {
        this.mediaListener = mediaListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "Select Audio "), REQUEST_CODE_AUDIO);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void getVideoFromGallary(MediaListener mediaListener) {
        this.mediaListener = mediaListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_CODE_PICK_VIDEO);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void getVideoFromCamera(MediaListener mediaListener) {
        this.mediaListener = mediaListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takeVideoIntent, REQUEST_CODE_PICK_VIDEO);
                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        }, Manifest.permission.CAMERA);
    }


    public void getPdfFromGallery(MediaListener mediaListener) {
        this.mediaListener = mediaListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivityForResult(Intent.createChooser(intent, "Select a file to upload"), REQUEST_CODE_GET_PDF);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void startActivityToCrop(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
//                Bitmap bMapCamera = getScaledDownBitmap((Bitmap) data.getExtras().get("output"));
                startActivityToCrop(cameraUri);
//                Bitmap bMapCamera = getScaledDownBitmap((Bitmap) data.getExtras().get("data"));
//                String pathFromURI = saveBitmapToFile(bMapCamera);
//                imageListener.onGetBitMap(bMapCamera, pathFromURI);
                break;

            case REQUEST_CODE_GALLARY:
                ArrayList<Image> images = new ArrayList<>();
                if (data.getData() != null) {
                    Uri uriImage = data.getData();
                    startActivityToCrop(uriImage);
////                    Bitmap bitmap = Util.getBitmapFromUri(activity, uriImage);
////                    String path = getFileUriFromContetnUri(getActivity(),uriImage);
//                    String path = getImagePathFromURI(uriImage);
//                    if (!TextUtils.isEmpty(path)) {
//                        File file = new File(path);
//                        if (file.exists()) {
//                            Image image = new Image(1, file.getName(), file.getPath(), true);
//                            images.add(image);
//                        }
//                    }
                } else {
                    ClipData clipData = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        clipData = data.getClipData();
                    }
                    int itemCount = clipData.getItemCount();
                    for (int i = 0; i < itemCount; i++) {
                        Uri uri = clipData.getItemAt(i).getUri();
                        String path = getImagePathFromURI(uri);
                        if (!TextUtils.isEmpty(path)) {
                            File file = new File(path);
                            if (file.exists()) {
                                Image imageArray = new Image(file.getName(), file.getPath());
                                images.add(imageArray);
                            }
                        }
                    }
                }
                imageListener.onGetImageArray(images);
//                startActivityToCrop(uriImage);
//                Bitmap bMap = getScaledDownBitmap(Util.getBitmapFromUri(activity, uriImage));
//                String path = saveBitmapToFile(bMap);
//                imageListener.onGetBitMap(bMap, path);
                break;
            case REQUEST_CODE_AUDIO:
                Uri uri = data.getData();
                mediaListener.onGetUri(uri, AUDIO, getAudioPathFromURI(uri));
                break;
            case REQUEST_CODE_PICK_VIDEO:
                Uri uriVideo = data.getData();
                mediaListener.onGetUri(uriVideo, VIDEO, getVideoPathFromURI(uriVideo));
                break;
            case REQUEST_CODE_GET_PDF:
                Uri uri1 = data.getData();
                String path2 = null;
                try {
                    path2 = Util.getFilePath(getActivity(), uri1);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                mediaListener.onGetUri(uri1, 10, path2);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    afterCrop(resultUri);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
                break;
        }
    }

    private void afterCrop(Uri imageUri) {
        Bitmap bitmap = Util.getBitmapFromUri(activity, imageUri);
        imageListener.onGetBitMap(bitmap, imageUri.toString());
    }

    public String getPDFPathFromURI(Uri contentUri) {
        String filePath;
        if (contentUri != null && "content".equals(contentUri.getScheme())) {
            Cursor cursor = getActivity().getContentResolver().query(contentUri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = contentUri.getPath();
        }
        return filePath;
    }

    public String getFileUriFromContetnUri(Context context, Uri _uri) {
        String filePath = null;
        Log.d("", "URI = " + _uri);
        if (_uri != null && "content".equals(_uri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(_uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = _uri.getPath();
        }
        return filePath;
    }

    public String getImagePathFromURI(Uri contentUri) {
        if (true) {
            return FileUtil.getPath(getContext(), contentUri);
        }
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
//        android.provider.MediaStore.Images.ImageColumns.DATA
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getVideoPathFromURI(Uri contentUri) {
        if (true) {
            return FileUtil.getPath(getContext(), contentUri);
        }
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getAudioPathFromURI(Uri contentUri) {
        if (true) {
            return FileUtil.getPath(getContext(), contentUri);
        }
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private Bitmap getScaledDownBitmap(Bitmap bMap) {
        return Bitmap.createScaledBitmap(bMap, 600, 600, true);
    }

    private void startCropper() {
        CropImage.activity(imageUri)
                .start(getContext(), this);
    }

    public void sendBoolean(boolean fromPostFeed) {

    }

    public void setImageListener(ImageListener imageListener) {
        this.imageListener = imageListener;
    }

    public void getMultipleImageFromGallery(ImageListener imageListener) {
        this.imageListener = imageListener;
        checkPerMission(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(intent, REQUEST_CODE_GALLARY);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> arrayList) {

            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public interface MediaListener {
        void onGetUri(Uri uri, int MediaType, String path);
    }

    public interface ImageListener {
        void onGetBitMap(Bitmap bitmap, String path);

        void onGetImageArray(ArrayList<Image> images);
    }

    public static String saveBitmapToFile(Bitmap bMap) {
        File file = Util.getOutputMediaFile();
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bMap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file.getPath();

    }

}

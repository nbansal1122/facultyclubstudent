package student.faqulty.club.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import student.faqulty.club.holders.BaseHolder;
import student.faqulty.club.model.BaseAdapterModel;

import java.util.List;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.TaskFragment;


/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseRecycleAdapter<T extends BaseAdapterModel> extends RecyclerView.Adapter<BaseHolder> implements TaskFragment.AsyncTaskListener {
    protected List<T> list;
    protected Context context;
    protected LayoutInflater inflater;
    protected AlertDialog dialog;
    private RecyclerClickInterface clickInterface;

    public RecyclerClickInterface getClickInterface() {
        return clickInterface;
    }

    public void setClickInterface(RecyclerClickInterface clickInterface) {
        this.clickInterface = clickInterface;
    }

    public BaseRecycleAdapter(Context context, List<T> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {

        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        holder.onBind(position, this.list.get(position));
    }


    @Override
    public int getItemViewType(int position) {
        return list.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public void onPreExecute(int taskCode) {
        showProgressBar();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        hideProgressBar();
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {

    }

    public void showProgressBar() {
        if (dialog != null && dialog.isShowing()) {
        } else {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading");
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void showToast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(int stringId) {
        Toast.makeText(context, stringId, Toast.LENGTH_LONG).show();
    }

    public static interface RecyclerClickInterface {
        public void onItemClick(View itemView, int position, Object obj, int actionType);
    }

}


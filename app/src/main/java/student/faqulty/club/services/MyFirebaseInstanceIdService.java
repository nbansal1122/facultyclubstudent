package student.faqulty.club.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by admin on 2/28/17.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG="Firebase";
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Preferences.saveData(AppConstants.PREF_KEYS.FCM_TOKEN,refreshedToken);
        Preferences.saveData(AppConstants.PREF_KEYS.IS_TOKEN_UPDATE,true);
    }
}

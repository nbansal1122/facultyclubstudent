package student.faqulty.club.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;


import student.faqulty.club.chatutil.SocketListener;
import student.faqulty.club.chatutil.SocketManager;
import student.faqulty.club.model.chat.ChatMessage;
import student.faqulty.club.receivers.InternetConnectivityReceiver;

import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 25/07/16.
 */
public class SocketService extends Service implements SocketListener, Loader.OnLoadCompleteListener<Cursor>, InternetConnectivityReceiver.InternetConnectListener {
    private SocketManager socketManager;
    private static final String TAG = "SocketService";
    private InternetConnectivityReceiver internetConnectivityReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private static boolean isStarted;

    public static void startService(Context ctx) {
        if (!isStarted)
            ctx.startService(new Intent(ctx, SocketService.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isStarted = true;
        SocketManager.init();
        socketManager = SocketManager.getInstance();
        socketManager.addListener(this);
        connectSocket();
        registerInternetBroadcast();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onMessageAdded(ChatMessage message) {

    }

    @Override
    public void onTypingStarted(String channelId, String userName) {

    }

    @Override
    public void onTypingEnded(String channelId, String userName) {

    }

    @Override
    public void onError(String error) {
        Log.d(TAG, "Error in socket" + error);
        if (Util.isConnectingToInternet(SocketService.this)) {
            connectSocket();
        }
    }

    @Override
    public void onConnectionError() {
        if (Util.isConnectingToInternet(SocketService.this)) {
            connectSocket();
        }
    }

    public static void connectSocket() {
        if (!SocketManager.getInstance().getSocket().connected()) {
            SocketManager.getInstance().getSocket().connect();
        }
    }

    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            Log.d("KSS", "data != null");
        } else {
            Log.d("KSS", "data = null");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isStarted = false;
        SocketManager.getInstance().close();
        if (internetConnectivityReceiver != null) {
            unregisterReceiver(internetConnectivityReceiver);
        }
    }

    public static void stopService(Context context) {
        context.stopService(new Intent(context, SocketService.class));
    }

    private void registerInternetBroadcast() {
        IntentFilter filter = new IntentFilter(InternetConnectivityReceiver.ACTION_CONNECTIVITY_CHANGE);
        if (internetConnectivityReceiver == null) {
            internetConnectivityReceiver = new InternetConnectivityReceiver(SocketService.this);
        }
        registerReceiver(internetConnectivityReceiver, filter);
    }


    @Override
    public void onInternetConnected() {
        connectSocket();
    }

    @Override
    public void onInternetDisconnected() {

    }
}

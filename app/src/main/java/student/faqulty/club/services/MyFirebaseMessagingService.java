package student.faqulty.club.services;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import student.faqulty.club.activity.SplashActivity;
import student.faqulty.club.model.push_notification.NotificationData;
import student.faqulty.club.util.AppController;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import simplifii.framework.utility.AppConstants;

/**
 * Created by admin on 2/28/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG="Fcm receiver";
    private int id;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        Map<String, String> data = remoteMessage.getData();
        if(data!=null){
            JSONObject jsonObject=new JSONObject();
            for(Map.Entry<String, String> stringStringEntry:data.entrySet()){
                try {
                    jsonObject.put(stringStringEntry.getKey(),stringStringEntry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            handleFCMData(jsonObject.toString());
        }

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if (notification != null) {
            Intent intent = new Intent(this, SplashActivity.class);
            AppController.sendNotification(intent,notification.getTitle(),notification.getBody(),id++);
        }
    }
    private void handleFCMData(String object) {
        NotificationData notificationData = new Gson().fromJson(object, NotificationData.class);
        if(notificationData!=null){
            Bundle bundle=new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT,notificationData);
            Intent intent = new Intent(this, SplashActivity.class);
            intent.putExtras(bundle);
            AppController.sendNotification(intent,notificationData.getTitle(),notificationData.getMessage(),notificationData.getType());
        }
    }

}

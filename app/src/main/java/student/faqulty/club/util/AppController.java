package student.faqulty.club.util;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import com.activeandroid.ActiveAndroid;
import student.faqulty.club.R;
import student.faqulty.club.activity.SplashActivity;
import student.faqulty.club.model.chat.ChatMessage;
import student.faqulty.club.model.chat.ChatText;
import student.faqulty.club.model.chat.MetaInfo;

import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by saurabh on 13-09-2016.
 */
public class AppController extends Application {
    private static AppController instance;
    public static long chatUserId;

    @Override
    public void onCreate() {
        super.onCreate();
        Preferences.initSharedPreferences(this);
        ActiveAndroid.initialize(this);
        instance=this;
    }
    public static void showNotification(ChatMessage chatMessage) {
        if (chatUserId != chatMessage.getFromUserID()) {
            String title = chatMessage.getFromUserName() + " message you";
            String content="";
            String messageType = chatMessage.getMessageType();
            switch (messageType) {
                case AppConstants.META_TYPES.TEXT:
                    ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
                    if(chatText!=null){
                        content=chatText.getText();
                    }
                    break;
                case AppConstants.META_TYPES.FILE:
                    content="Image";
                    break;
            }
            Intent intent = new Intent(instance, SplashActivity.class);
            sendNotification(intent,title,content, (int)chatUserId);
        }
    }

    public static AppController getInstance() {
        return instance;
    }

    public static void sendNotification(Intent intent, String title, String content, int id) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(instance, id,
                intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bm = BitmapFactory.decodeResource(instance.getResources(), R.mipmap.fc_icon);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new
                NotificationCompat.Builder(instance)
                .setSmallIcon(R.mipmap.fc_icon)
                .setContentTitle(title)
                .setLargeIcon(bm)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(content))
                .setContentText(content)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(content))
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) instance.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notificationBuilder.build());
    }

}

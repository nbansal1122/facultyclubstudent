package student.faqulty.club.chatutil;

import android.content.Context;
import android.widget.Toast;


import student.faqulty.club.model.chat.ChatMessage;

import io.socket.client.Socket;
import simplifii.framework.utility.Util;

/**
 * Created by admin on 2/24/17.
 */

public class SocketUtil {
    public static synchronized void sendChatMessage(ChatMessage chatMessage, Context context){
        if(!Util.isConnectingToInternet(context)){
            Toast.makeText(context, "No internet connected !", Toast.LENGTH_SHORT).show();
            return;
        }
        SocketManager socketManager = SocketManager.getInstance();
        if(socketManager.isConnected()){
            Socket socket = socketManager.getSocket();
            socketManager.emmitEvent(SocketManager.MESSAGE,chatMessage);
        }
    }
}

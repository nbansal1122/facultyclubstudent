package student.faqulty.club.chatutil;


import student.faqulty.club.model.chat.ChatMessage;

public interface SocketListener {
    void onMessageAdded(ChatMessage message);
    void onTypingStarted(String channelId, String userName);
    void onTypingEnded(String channelId, String userName);
    void onError(String error);
    void onConnectionError();
}
package student.faqulty.club.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import student.faqulty.club.R;

import simplifii.framework.activity.BaseActivity;

public class MainActivity extends BaseActivity {
    private Button btn_signup, btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btn_signup = (Button) findViewById(R.id.btn_signup);
        btn_login = (Button) findViewById(R.id.btn_login);
        setOnClickListener(R.id.btn_signup,R.id.btn_login);
    }

    @Override
    public void onClick(View v) {
        setUnselectAll();
        switch (v.getId()){
            case R.id.btn_signup:
                setSelect(btn_signup);
                startNextActivity(SignupActivity.class);
                finish();
                break;
            case R.id.btn_login:
                setSelect(btn_login);
                startNextActivity(LoginActivity.class);
                finish();
                break;
        }
        super.onClick(v);
    }

    private void setUnselectAll() {
        btn_login.setBackgroundColor(getResourceColor(R.color.white));
        btn_login.setTextColor(getResourceColor(R.color.orange));
        btn_signup.setBackgroundColor(getResourceColor(R.color.white));
        btn_signup.setTextColor(getResourceColor(R.color.orange));
    }

    public void setSelect(Button select) {
        select.setTextColor(getResourceColor(R.color.white));
        select.setBackgroundColor(getResourceColor(R.color.orange));
    }
}

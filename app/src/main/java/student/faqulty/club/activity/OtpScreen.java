package student.faqulty.club.activity;

import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import student.faqulty.club.R;
import student.faqulty.club.receivers.SMSListener;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import student.faqulty.club.model.BaseApi;
import student.faqulty.club.model.LoginResponse;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class OtpScreen extends BaseActivity implements SMSListener.OTPCallback {
    private Button submit;
    private EditText otp;
    private TextView mobno, resendOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_screen);
        initToolBar("Enter OTP");
        submit = (Button) findViewById(R.id.btn_submit_otp);
        otp = (EditText) findViewById(R.id.et_otp);
        mobno = (TextView) findViewById(R.id.tv_mob_no);
        resendOTP = (TextView) findViewById(R.id.resend_otp);
        setOnClickListener(R.id.btn_submit_otp, R.id.resend_otp);
        mobno.setText(Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, "0"));
        askPermissionToReceiveSMS();
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_submit_otp:
                verifyOTP();
                break;
            case R.id.resend_otp:
                resendOTP();
                break;
        }
    }

    private void verifyOTP() {
        if (isValidate(R.id.et_otp)) {
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.OTP_VERIFY);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", otp.getText().toString());
                jsonObject.put("phone", Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            httpParamObject.setPostMethod();
            httpParamObject.setJson(jsonObject.toString());
            httpParamObject.setContentType("application/json");
            executeTask(AppConstants.TASKCODES.OTP, httpParamObject);
        }
    }

    public void resendOTP() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SIGNUP);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, ""));
            jsonObject.put("password", Preferences.getData(AppConstants.PREF_KEYS.KEY_PASSWORD, ""));
            jsonObject.put("confirmPassword", Preferences.getData(AppConstants.PREF_KEYS.KEY_PASSWORD, ""));
            jsonObject.put("source", "local");
            jsonObject.put("userType", AppConstants.USER_TYPE_STUDENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setClassType(BaseApi.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setContentType("application/json");
        executeTask(AppConstants.TASKCODES.RESEND_OTP, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.OTP:
                Bundle extra = getIntent().getExtras();
                if (extra != null) {
                    if (extra.getBoolean(AppConstants.BUNDLE_KEYS.SOCIALSIGNUP)) {
                        setResult(RESULT_OK);
                        finish();
                        return;
                    }
                }
                loginUser();
                break;
            case AppConstants.TASKCODES.RESEND_OTP:
                BaseApi baseApi = (BaseApi) response;
                showToast(baseApi.getMsg());
                break;
            case AppConstants.TASKCODES.LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    LoginResponse.saveUserData(loginResponse);
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }

    private void loginUser() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.LOGIN);
        httpParamObject.setClassType(LoginResponse.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(getJsonData().toString());
        httpParamObject.setContentType("application/json");
        executeTask(AppConstants.TASKCODES.LOGIN, httpParamObject);
    }

    private JSONObject getJsonData() {
        String password = Preferences.getData(AppConstants.PREF_KEYS.KEY_PASSWORD, "");
        String mobileNumber = Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", mobileNumber);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

//    private void getUserData(LoginResponse loginResponse) {
//        UserDetailsApi user;
//        HttpParamObject obj = new HttpParamObject();
//        obj.setUrl(AppConstants.PAGE_URL.GET_USER + loginResponse.getUserId());
//        obj.setClassType(UserDetailsApi.class);
//        obj.addParameter("scope", "detailed");
//        executeTask(AppConstants.TASKCODES.GET_USER, obj);
//
//    }

    private void askPermissionToReceiveSMS() {
        new TedPermission(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        registerOTPReceiver();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> arrayList) {
//                        registerReceiver();
                    }
                })
                .setDeniedMessage("Permission required by Faqulty Club to verify otp")
                .setPermissions(android.Manifest.permission.RECEIVE_SMS)
                .setRationaleMessage("Permission required by Faqulty Club to verify otp")
                .check();
    }

    private SMSListener smsListener;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (smsListener != null) {
            unregisterReceiver(smsListener);
        }
    }

    private void registerOTPReceiver() {
        if (smsListener == null) {
            smsListener = new SMSListener(this);
        }
        registerReceiver(smsListener, getFilter());
    }

    private IntentFilter getFilter() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        return filter;
    }


    @Override
    public void onOTPReceived(String otpString) {
        otp.setText(otpString);
        verifyOTP();
    }
}
package student.faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;

import student.faqulty.club.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreenWindow();
        setContentView(R.layout.activity_splash);
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (Preferences.getData(AppConstants.PREF_KEYS.IS_LOGIN, false)) {
                            if(Preferences.getData(AppConstants.PREF_KEYS.IS_PROFILE_COMPLETE,true)){
                                startNextActivity(HomeActivity.class);
                                finish();
                            }else {
                                Intent i = new Intent(SplashActivity.this, CompleteProfileActivity.class);
                                startActivity(i);
                                finish();
                            }

                        }else{
                            Intent i = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                });
            }
        }.start();

    }
}

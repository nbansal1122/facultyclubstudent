package student.faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import student.faqulty.club.R;
import student.faqulty.club.model.notifications.Notification;
import student.faqulty.club.model.notifications.NotificationData;
import student.faqulty.club.model.notifications.NotificationResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class NotificationActivity extends BaseActivity implements CustomListAdapterInterface {
    List<Notification> notificationList = new ArrayList<>();
    private CustomListAdapter customListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initToolBar(getString(R.string.app_name));
        ListView listView = (ListView) findViewById(R.id.lv_notification);
        customListAdapter = new CustomListAdapter(this, R.layout.row_notification, notificationList, this);
        listView.setAdapter(customListAdapter);
        listView.setEmptyView(findViewById(R.id.empty_view));
    }

//    @Override
//    protected int getHomeIcon() {
//        return R.mipmap.fc_logo;
//    }
//
//    @Override
//    protected void onHomePressed() {
//
//    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getNotifications();
    }

    private void getNotifications() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GET_NOTIFICATIONS, Preferences.getUserId()));
        httpParamObject.setClassType(NotificationResponse.class);
        executeTask(AppConstants.TASKCODES.GET_NOTIFICATION, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_NOTIFICATION:
                NotificationResponse notificationResponse = (NotificationResponse) response;
                NotificationData data = notificationResponse.getData();
                if (data != null) {
                    List<Notification> notifications = data.getNotifications();
                    if (notifications != null) {
                        notificationList.addAll(notifications);
                        customListAdapter.notifyDataSetChanged();
                        setText(getString(R.string.notification) + " ( " + data.getNotifications().size() + " )", R.id.tv_notification);
                    }

                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        Notification notification = notificationList.get(position);
        holder.tvMessage.setText(notification.getMessage());
        String createdOn = notification.getCreatedOn();
        if (!TextUtils.isEmpty(createdOn)) {
            String timeString = Util.getRelativeTimeString(createdOn);
            holder.tvTime.setText(timeString);
        }
        return convertView;
    }

    class Holder {
        ImageView ivIcon;
        TextView tvMessage, tvTime;

        public Holder(View view) {
            ivIcon = (ImageView) view.findViewById(R.id.iv_pic);
            tvMessage = (TextView) view.findViewById(R.id.tv_msg);
            tvTime = (TextView) view.findViewById(R.id.tv_time);
        }
    }
}

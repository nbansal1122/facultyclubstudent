package student.faqulty.club.activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import student.faqulty.club.R;

import org.json.JSONException;
import org.json.JSONObject;

import student.faqulty.club.model.BaseApi;
import student.faqulty.club.model.user.UserDetailsApi;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class SignupActivity extends BaseActivity {
    private EditText signup_password, mobile_no;
    private String mobileno, password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initToolBar(getString(R.string.sign_up));

        mobile_no = (EditText) findViewById(R.id.et_mobile_number);
        signup_password = (EditText) findViewById(R.id.password);
//        signup_confirm_password = (TextInputLayout) findViewById(R.id.confirm_pasword);
        setOnClickListener(R.id.sign_up_btn);

    }


    private boolean isValid() {
//        clearTilError(R.id.til_mobile_number, R.id.password);
        mobileno = getEditText(R.id.et_mobile_number);
        password = getEditText(R.id.password);
        if (!TextUtils.isEmpty(mobileno) && mobileno.length() == 10) {
        } else {
            showToast(getString(R.string.error_invalid_mobile));
            return false;
        }

        if (!TextUtils.isEmpty(password) && password.length() >= 4) {
        } else {
            showToast(getString(R.string.error_password));
            return false;
        }
//        if (confirmpassword.equals(password)) {
//        } else {
//            showToast(getString(R.string.error_confirm_password));
//            return false;
//        }
        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sign_up_btn:
                if (isValid()) {
                    signUpUser();
                }
                break;
        }
    }

    private JSONObject getSignupData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", mobileno);
            jsonObject.put("password", password);
            jsonObject.put("confirmPassword", password);
            jsonObject.put("source", "local");
            jsonObject.put("userType", AppConstants.USER_TYPE_STUDENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("signup", jsonObject.toString());
        return jsonObject;
    }

    private void signUpUser() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.SIGNUP);
        httpParamObject.setClassType(BaseApi.class);
        httpParamObject.setPostMethod();
        httpParamObject.setJson(getSignupData().toString());
        httpParamObject.setContentType("application/json");
        executeTask(AppConstants.TASKCODES.SIGNUP, httpParamObject);
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        showToast("Background error");
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (null == response) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.SIGNUP:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi == null) {
                    showToast(R.string.server_error);
                    return;
                }
                showToast(baseApi.getMsg());
                Preferences.saveData(AppConstants.PREF_KEYS.PHONE_NO, mobileno);
                Preferences.saveData(AppConstants.PREF_KEYS.KEY_PASSWORD, password);
                startOTP();
                break;
            case AppConstants.TASKCODES.GET_USER:
                UserDetailsApi api = (UserDetailsApi) response;
                if (api != null) {
                    moveToProfileCompleteActivity();
                }
                break;
        }
    }

    private void startOTP() {
        Intent i = new Intent(SignupActivity.this, OtpScreen.class);
        i.putExtra(AppConstants.BUNDLE_KEYS.SOCIALSIGNUP, false);
        startActivityForResult(i, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN,true);
            getUserBasicProfile();
        } else {
            showToast(getString(R.string.verify_mobile_to_proceed));
        }

    }

    private void getUserBasicProfile() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER + Preferences.getData(AppConstants.PREF_KEYS.USER_ID,(long)450));
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, httpParamObject);
    }

    private void moveToProfileCompleteActivity() {
        Intent i = new Intent(this, CompleteProfileActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onBackPressed() {
        startNextActivity(MainActivity.class);
        finish();
    }

}

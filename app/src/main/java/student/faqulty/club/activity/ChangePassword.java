package student.faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import student.faqulty.club.R;

import org.json.JSONException;
import org.json.JSONObject;

import student.faqulty.club.model.user.UpdatePassword;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class ChangePassword extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initToolBar(AppConstants.PAGE_TITLES.CHANGE_PASSWORD);

        setOnClickListener(R.id.btn_change_pass);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_change_pass:
                if (isValid()) {
                    changePassword();
                }
        }
    }

    private boolean isValid() {
        String mobile = Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, "");
        String password = getEditText(R.id.et_new_password);
        String confirmPass = getEditText(R.id.et_confirm_password);
        String currPass = getEditText(R.id.et_current_password);
        if (TextUtils.isEmpty(mobile) || mobile.length() != 10){
            showToast("Please correct mobile number");
            return false;
        }
        if(TextUtils.isEmpty(password) || password.length()<4){
            showToast("Please correct password");
            return false;
        }
        if(!password.equals(confirmPass)){
            showToast("Password mismatch");
            return false;
        }
        if(TextUtils.isEmpty(currPass)){
            showToast("Please enter current password");
            return false;
        }
            return true;
    }

    private void changePassword() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.RESET_PASSWORD);
        httpParamObject.setClassType(UpdatePassword.class);
        httpParamObject.setJson(getData().toString());
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        executeTask(AppConstants.TASKCODES.RESET_PASSWORD, httpParamObject);

    }

    private JSONObject getData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", Preferences.getData(AppConstants.PREF_KEYS.PHONE_NO, ""));
            jsonObject.put("password", getEditText(R.id.et_new_password));
            jsonObject.put("confirmPassword", getEditText(R.id.et_confirm_password));
            jsonObject.put("currentPassword", getEditText(R.id.et_current_password));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response == null){
            showToast("No response");
            return;
        }
        switch (taskCode){
            case AppConstants.TASKCODES.RESET_PASSWORD:
                UpdatePassword updatePassword = (UpdatePassword) response;
                if(updatePassword!=null){
                    showToast(updatePassword.getMsg());
                    finish();
                }
                break;
        }
    }
}

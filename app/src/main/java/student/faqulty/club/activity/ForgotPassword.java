package student.faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import student.faqulty.club.R;

import org.json.JSONException;
import org.json.JSONObject;

import student.faqulty.club.model.BaseApi;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class ForgotPassword extends BaseActivity {
    Button submit;
    EditText mobileno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initToolBar("Forgot Password");
        getHomeIcon(R.mipmap.arrows);

        mobileno = (EditText) findViewById(R.id.et_mobile_number);

        setOnClickListener(R.id.btn_submit);
    }

    private int getHomeIcon(int arrows) {
        return R.mipmap.arrows;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_submit:
                if (isValid()) {
                    HttpParamObject httpParamObject = new HttpParamObject();
                    httpParamObject.setUrl(AppConstants.PAGE_URL.FORGOT_PASSWORD);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("phone", mobileno.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    httpParamObject.setPostMethod();
                    httpParamObject.setClassType(BaseApi.class);
                    httpParamObject.setJson(jsonObject.toString());
                    httpParamObject.setContentType("application/json");
                    executeTask(AppConstants.TASKCODES.FORGOT_PASSWORD, httpParamObject);
                }
        }
    }

    private boolean isValid() {
        String mobile = getEditText(R.id.et_mobile_number);
        if (!TextUtils.isEmpty(mobile) && mobile.length() == 10) {
        } else {
            showToast(R.string.error_invalid_mobile);
            return false;
        }
        return true;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.FORGOT_PASSWORD:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi != null) {
                    showToast(baseApi.getMsg());
                    Intent i = new Intent(this, ResetPassword.class);
                    i.putExtra("mobno", mobileno.getText().toString().trim());
                    startActivity(i);
//                    finish();
                } else {
                    showToast("Please try again");
                }
        }
    }
}

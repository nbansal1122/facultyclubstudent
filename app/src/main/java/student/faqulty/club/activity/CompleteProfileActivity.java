package student.faqulty.club.activity;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import student.faqulty.club.Fragments.AddImageFragment;
import student.faqulty.club.Fragments.MediaFragment;
import student.faqulty.club.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import student.faqulty.club.model.Image;
import student.faqulty.club.model.UploadImageData;
import student.faqulty.club.model.UploadImageResponse;
import student.faqulty.club.model.user.UserDetailsApi;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class CompleteProfileActivity extends BaseActivity {
    private Button btn_update;
    private TextView tv_date, tv_terms;
    private int mDay, mMonth, mYear;
    private MediaFragment mediaFragment;
    private Bitmap profileBitmap;
    private ImageView profilePic;
    String profilePicUri = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profile);
        Preferences.saveData(AppConstants.PREF_KEYS.IS_PROFILE_COMPLETE,false);
        initToolBar(AppConstants.PAGE_TITLES.COMPLETE_PROFILE);
        btn_update = (Button) findViewById(R.id.btn_update_profile);
        tv_date = (TextView) findViewById(R.id.tv_dob);
        tv_terms = (TextView) findViewById(R.id.tv_terms_n_policy);
        profilePic = (ImageView) findViewById(R.id.iv_profilePic);
        SpannableString spannableString = new SpannableString(getString(R.string.by_sigining_up_you_agree_to_all_terms_and_services_and_privacy_policy));
        spannableString.setSpan(new ForegroundColorSpan(getResourceColor(R.color.black)), 33, 51, 0);
        spannableString.setSpan(new ForegroundColorSpan(getResourceColor(R.color.black)), 55, 70, 0);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 33, 51, 0);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 55, 70, 0);
        tv_terms.setMovementMethod(LinkMovementMethod.getInstance());
        tv_terms.setText(spannableString);

        mediaFragment = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "Profile Pic").commit();

        setOnClickListener(R.id.btn_update_profile, R.id.tv_dob, R.id.iv_edit_pic);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update_profile:
                String email = getEditText(R.id.et_email);
                if(TextUtils.isEmpty(email)){
                    showToast(R.string.error_empty_email);
                    return;
                }
                if(!Util.isValidEmail(email)){
                    showToast(R.string.error_invalid_email);
                    return;
                }

                String name = getEditText(R.id.et_name);
                if(TextUtils.isEmpty(name)){
                    showToast("Name cannot be empty !");
                    return;
                }
                if (profileBitmap != null)
                    uploadImageData();
                else
                    updateProfileData();
                break;
            case R.id.tv_dob:
                final Calendar calendar = Calendar.getInstance();
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(CompleteProfileActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String formatedDate = Util.getFormatedDate(dayOfMonth, monthOfYear, year, "yyyy-MM-dd");
                                tv_date.setText(formatedDate);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
                break;
            case R.id.iv_edit_pic:
                getImage();
                break;
        }

        super.onClick(v);
    }

    private void getImage() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.share_image));
        mediaFragment.setImageListener(new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                profilePic.setImageBitmap(bitmap);
                profileBitmap=bitmap;
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                if (images.size() > 0) {
                    String path = images.get(0).getFilePath();
                }
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(mediaFragment);
        bottomSheetDialogFragment.setArguments(bundle);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    private void uploadImageData() {
        try {
            File file = Util.getFile(profileBitmap, "ProfilePicFaqultyClub");
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProfileData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_USER + Preferences.getData(AppConstants.PREF_KEYS.USER_ID, (long) 452) + "?type=basic");
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setPutMethod();
        httpParamObject.setJson(getBasicData().toString());
        executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);
    }

    private JSONObject getBasicData() {
        JSONObject jsonObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(profilePicUri)) {
                jsonObject.put("image", profilePicUri);
            } else {
                jsonObject.put("image", "");
            }
            jsonObject.put("name", getEditText(R.id.et_name));
            jsonObject.put("email", getEditText(R.id.et_email));
            jsonObject.put("dateOfBirth", getTvData(R.id.tv_dob));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private String getTvData(int id) {
        TextView tv = (TextView) findViewById(id);
        if (tv != null)
            return tv.getText().toString();
        return "";
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;
                if (userDetailsApi != null) {
                    Preferences.saveData(AppConstants.PREF_KEYS.IS_PROFILE_COMPLETE,true);
                    showToast("Profile data updated");
                    startNextActivity(HomeActivity.class);
                    finish();
                }
                break;
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    UploadImageData data = uploadImageResponse.getData();
                    if (data != null) {
                        profilePicUri = data.getUri();
                        updateProfileData();
                    }
                }
                break;
        }
    }
}

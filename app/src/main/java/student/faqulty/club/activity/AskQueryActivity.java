package student.faqulty.club.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;


import student.faqulty.club.R;
import student.faqulty.club.model.BaseApi;
import student.faqulty.club.model.user.UserDetailsApi;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class AskQueryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_query);
        initToolBar("Ask Query");
        setFreFilledData();
        setOnClickListener(R.id.btn_submit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                String name = getEditText(R.id.et_name).trim();
                if (TextUtils.isEmpty(name)) {
                    showToast(R.string.error_empty_name);
                    return;
                }
                String email = getEditText(R.id.et_email).trim();
                if (TextUtils.isEmpty(email)) {
//                    showToast(R.string.error_empty_email);
//                    return;
                } else if (!Util.isValidEmail(email)) {
                    showToast(R.string.invalid_email);
                    return;
                }
                String query = getEditText(R.id.et_query).trim();
                if (TextUtils.isEmpty(query)) {
                    showToast(R.string.error_empty_query);
                    return;
                }
                submitData(name, email, query);
                break;
        }
    }

    private void submitData(String name, String email, String query) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("about", query);
            jsonObject.put("phoneNumber", UserDetailsApi.getInstance().getPhone());
            jsonObject.put("userId", UserDetailsApi.getInstance().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.ASK_QUERY);
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BaseApi.class);
        executeTask(AppConstants.TASKCODES.ASK_QUERY, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASKCODES.ASK_QUERY:
                BaseApi baseApi = (BaseApi) response;
                if (baseApi != null) {
                    showToast(baseApi.getMsg());
                    finish();
                }
                break;
        }
    }

    private void setFreFilledData() {
        UserDetailsApi userDetailsApi = UserDetailsApi.getInstance();
        if (userDetailsApi != null) {
            setText(userDetailsApi.getName(), R.id.et_name);
            setText(userDetailsApi.getEmail(), R.id.et_email);
        }
    }
}

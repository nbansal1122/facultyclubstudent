package student.faqulty.club.activity;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import student.faqulty.club.Fragments.AddImageFragment;
import student.faqulty.club.Fragments.MediaFragment;
import student.faqulty.club.R;
import student.faqulty.club.model.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import student.faqulty.club.model.UploadImageData;
import student.faqulty.club.model.UploadImageResponse;
import student.faqulty.club.model.user.UserDetailsApi;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class ProfileDetails extends BaseActivity {
    private TextView tv_change_pass, tv_date;
    private int mYear, mMonth, mDay;
    private UserDetailsApi userDetailsApi;
    private MediaFragment mediaFragment;
    private Bitmap profileBitmap;
    private ImageView profilePic;
    private String imageUrl;
    String profilePicUri ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);
        initToolBar(AppConstants.PAGE_TITLES.EDIT_PROFILE);
        userDetailsApi = UserDetailsApi.getInstance();
        profilePic = (ImageView) findViewById(R.id.iv_profilePic);
        if (userDetailsApi == null) {
            getUserBasicProfile();
        } else {
            setData();
        }
        tv_change_pass = (TextView) findViewById(R.id.tv_change_pass);
        tv_date = (TextView) findViewById(R.id.tv_date_details);
        mediaFragment = new MediaFragment();
        getSupportFragmentManager().beginTransaction().add(mediaFragment, "Profile Pic").commit();
        setOnClickListener(R.id.tv_change_pass, R.id.tv_date_details, R.id.iv_edit_pic);
    }

    private void getUserBasicProfile() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER + Preferences.getData(AppConstants.PREF_KEYS.USER_ID, (long) 0));
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_USER:
                UserDetailsApi api = (UserDetailsApi) response;
                if (api != null) {
                    setData();
                }
                break;
            case AppConstants.TASKCODES.UPDATE_USER:
                UserDetailsApi userDetailsApi = (UserDetailsApi) response;

                if (userDetailsApi != null) {
                    showToast("Profile data updated");
                    finish();
                    getUserBasicProfile();
                }
                break;
            case AppConstants.TASKCODES.UPLOAD_IMAGE:
                UploadImageResponse uploadImageResponse = (UploadImageResponse) response;
                if (uploadImageResponse != null) {
                    UploadImageData data = uploadImageResponse.getData();
                    if (data != null) {
                        profilePicUri = data.getUri();
                        uploadUserData();
                    }
                }
                break;
        }
    }

    private void setData() {
        if (userDetailsApi != null) {
            if (null != userDetailsApi.getName())
                setEditText(R.id.et_name, userDetailsApi.getName());
            if (null != userDetailsApi.getEmail())
                setEditText(R.id.et_email, userDetailsApi.getEmail());
            if (null != userDetailsApi.getPhone())
                setEditText(R.id.et_mobile, userDetailsApi.getPhone());
            if (null != userDetailsApi.getDateOfBirth())
                setText(userDetailsApi.getFormatedDateOfBirth(), R.id.tv_date_details);
            if (!TextUtils.isEmpty(userDetailsApi.getImage())) {
                profilePicUri = userDetailsApi.getCompleteImageUrl();
                Picasso.with(this).load(profilePicUri).placeholder(R.mipmap.accountselected2x).into(profilePic);
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_change_pass:
                startNextActivity(ChangePassword.class);
                break;
            case R.id.tv_date_details:
                final Calendar calendar = Calendar.getInstance();
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileDetails.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                tv_date.setText(Util.getFormatedDate(dayOfMonth, monthOfYear, year, "dd-MM-yyyy"));
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
                break;
            case R.id.iv_edit_pic:
                getImage();
                break;
        }
        super.onClick(v);
    }
    private void getImage() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_KEYS.FRAGMENT_MESSAGE, getString(R.string.share_image));
        mediaFragment.setImageListener(new MediaFragment.ImageListener() {
            @Override
            public void onGetBitMap(Bitmap bitmap, String path) {
                profilePic.setImageBitmap(bitmap);
                profileBitmap=bitmap;
            }

            @Override
            public void onGetImageArray(ArrayList<Image> images) {
                if (images.size() > 0) {
                    String path = images.get(0).getFilePath();
                }
            }
        });
        BottomSheetDialogFragment bottomSheetDialogFragment = AddImageFragment.getInctance(mediaFragment);
        bottomSheetDialogFragment.setArguments(bundle);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    private void validateData() {
        String name = getEditText(R.id.et_name);
        String email = getEditText(R.id.et_email);

        if (TextUtils.isEmpty(name)) {
            showToast(R.string.error_empty_name);
            return;
        }
        if (TextUtils.isEmpty(email)) {
            showToast(R.string.error_empty_email);
            return;
        }
        if (!Util.isValidEmail(email)) {
            showToast(R.string.error_invalid_email);
            return;
        }
        if (profileBitmap != null) {
            uploadProfilePic();
        } else {
            uploadUserData();
        }
    }

    private void uploadProfilePic() {
        try {
            File file = Util.getFile(profileBitmap, getString(R.string.app_name));
            FileParamObject fileParamObject = new FileParamObject(file, file.getName(), "file");
            fileParamObject.setUrl(AppConstants.PAGE_URL.UPLOAD_IMAGE);
            fileParamObject.setPostMethod();
            fileParamObject.setClassType(UploadImageResponse.class);
            executeTask(AppConstants.TASKCODES.UPLOAD_IMAGE, fileParamObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadUserData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.UPDATE_USER + Preferences.getData(AppConstants.PREF_KEYS.USER_ID, (long) 0) + "?type=basic");
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.setJSONContentType();
        httpParamObject.setJson(getBasicData().toString());
        httpParamObject.setPutMethod();
        executeTask(AppConstants.TASKCODES.UPDATE_USER, httpParamObject);

    }

    private JSONObject getBasicData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("image", profilePicUri);
            jsonObject.put("name", getEditText(R.id.et_name));
            jsonObject.put("email", getEditText(R.id.et_email));
            jsonObject.put("dateOfBirth", Util.convertDateFormat(getTextView(R.id.tv_date_details),"dd-MM-yyyy","yyyy-MM-dd"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_update:
                validateData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

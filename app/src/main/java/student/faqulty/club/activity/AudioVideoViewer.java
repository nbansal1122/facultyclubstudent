package student.faqulty.club.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import student.faqulty.club.R;

import nz.co.delacour.exposurevideoplayer.ExposureVideoPlayer;
import simplifii.framework.utility.AppConstants;

public class AudioVideoViewer extends AppCompatActivity {

    private ExposureVideoPlayer videoView;

    public static void startActivity(Context context, String url, String mimeType) {
        Intent i = new Intent(context, AudioVideoViewer.class);
        i.putExtra(AppConstants.BUNDLE_KEYS.KEY_URL, url);
        i.putExtra(AppConstants.BUNDLE_KEYS.MIME_TYPE, mimeType);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_video_viewer);
        String url = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.KEY_URL);
        videoView = (ExposureVideoPlayer) findViewById(R.id.videoView_content);
        videoView.init(this);
        if(!TextUtils.isEmpty(url)){
            videoView.setVideoSource(url);
        }
        videoView.setFullScreen(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (videoView.isPlaying())
            videoView.stop();
    }
}

package student.faqulty.club.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import student.faqulty.club.R;

import org.json.JSONException;
import org.json.JSONObject;

import student.faqulty.club.model.LoginResponse;
import student.faqulty.club.model.user.UserDetailsApi;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class LoginActivity extends BaseActivity {
    Button login;
    EditText et_mobileno, et_password;
    TextView forgotPass;
    private LoginResponse loginResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initToolBar("Login");
        getHomeIcon();

        setOnClickListener(R.id.login_btn, R.id.tv_forgot_password);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.login_btn:
                if (isValid()) {
                    HttpParamObject httpParamObject = new HttpParamObject();
                    httpParamObject.setUrl(AppConstants.PAGE_URL.LOGIN);
                    httpParamObject.setClassType(LoginResponse.class);
                    httpParamObject.setPostMethod();
                    httpParamObject.setJson(getJsonData().toString());
                    httpParamObject.setContentType("application/json");
                    executeTask(AppConstants.TASKCODES.LOGIN, httpParamObject);
                }
                break;
            case R.id.tv_forgot_password:
                startNextActivity(ForgotPassword.class);

        }
    }

    private boolean isValid() {
//        clearTilError(R.id.til_mobile_number, R.id.til_password);
        String mobile = getEditText(R.id.et_mobile_number);
        String password = getEditText(R.id.password);
        if (!TextUtils.isEmpty(mobile) && mobile.length() == 10) {
        } else {
            showToast(getString(R.string.error_invalid_mobile));
            return false;
        }

        if (!TextUtils.isEmpty(password) && password.length() >= 4) {
        } else {
            showToast(getString(R.string.error_password));
            return false;
        }
        return true;
    }


    private JSONObject getJsonData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", getEditText(R.id.et_mobile_number));
            jsonObject.put("password", getEditText(R.id.password));
            saveData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void saveData() {
        Preferences.saveData(AppConstants.PREF_KEYS.PHONE_NO,getEditText(R.id.et_mobile_number));
    }


    //// TODO: toast message on empty field

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(null == response){
            showToast("No response");
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.LOGIN:
                LoginResponse loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    saveUserData(loginResponse);
                    getUserData(loginResponse);
                } else {
                    showToast("Please try again.");
                }
                break;
            case AppConstants.TASKCODES.GET_USER:
                moveToNextActivity();
                break;
        }
    }

    public static void saveUserData(LoginResponse loginResponse) {
        Preferences.saveData(AppConstants.PREF_KEYS.USER_TOKEN, loginResponse.getToken());
        Preferences.saveData(AppConstants.PREF_KEYS.USER_ID, loginResponse.getUserId());
        Preferences.saveData(Preferences.LOGIN_KEY, true);
    }

    private void moveToNextActivity() {
        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(i);
        Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, true);
        finish();
    }

    private void getUserData(LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.GET_USER + loginResponse.getUserId());
        httpParamObject.setClassType(UserDetailsApi.class);
        httpParamObject.addParameter("scope", "detailed");
        executeTask(AppConstants.TASKCODES.GET_USER, httpParamObject);
    }

    @Override
    public void onBackPressed() {
        startNextActivity(MainActivity.class);
        finish();
    }
}

package student.faqulty.club.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;

import student.faqulty.club.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import student.faqulty.club.chat.IndividualChatActivity;
import student.faqulty.club.model.BaseApi;
import student.faqulty.club.model.FcmTokenUpdateResponse;
import student.faqulty.club.model.chat.ChatMessage;
import student.faqulty.club.model.chat.ChatText;
import student.faqulty.club.model.chat.MetaInfo;
import student.faqulty.club.model.group.GroupData;
import student.faqulty.club.model.group.GroupResponse;
import student.faqulty.club.model.tutor.TutorResponse;
import student.faqulty.club.services.SocketService;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class HomeActivity extends BaseActivity implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    List<BaseApi> homeList = new ArrayList<>();
    private Map<Integer, ChatMessage> lastMessageMap = new HashMap<>();
    ListView lv_home;
    CustomListAdapter customListAdapter;
    private boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        updateFcmToken();
        initToolBar("");
        lv_home = (ListView) findViewById(R.id.lv_home);
        customListAdapter = new CustomListAdapter(this, R.layout.row_home, homeList, this);
        lv_home.setAdapter(customListAdapter);
        lv_home.setOnItemClickListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getTutors();
        getGroups();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        getTutors();
        getGroups();
    }

    private void updateFcmToken() {
        if (Preferences.getData(AppConstants.PREF_KEYS.IS_TOKEN_UPDATE, true)) {
            String tkn = FirebaseInstanceId.getInstance().getToken();
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.FCM_UPDATE_TOKEN);
            httpParamObject.setPostMethod();
            httpParamObject.setJSONContentType();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userId", Preferences.getUserId());
                jsonObject.put("userType", AppConstants.USER_TYPES.STUDENT);
                jsonObject.put("fcmToken", tkn);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            httpParamObject.setJson(jsonObject.toString());
            httpParamObject.setClassType(FcmTokenUpdateResponse.class);
            executeTask(AppConstants.TASKCODES.UPDATE_TOKEN, httpParamObject);
        }
    }

    private void getGroups() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GET_GROUP, Preferences.getData(AppConstants.PREF_KEYS.USER_ID, (long) 0)));
        httpParamObject.setClassType(GroupResponse.class);
        executeTask(AppConstants.TASKCODES.GET_GROUP_RESPONSE, httpParamObject);
    }

    private void getTutors() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(String.format(AppConstants.PAGE_URL.GET_TUTORS, Preferences.getUserId()));
        httpParamObject.setClassType(TutorResponse.class);
        executeTask(AppConstants.TASKCODES.GET_TUTOR_RESPONSE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response == null) {
            showToast(R.string.server_error);
            return;
        }
        switch (taskCode) {
            case AppConstants.TASKCODES.GET_TUTOR_RESPONSE:
                List<TutorResponse> tutorResponseList = (List<TutorResponse>) response;
                homeList.addAll(tutorResponseList);
                customListAdapter.notifyDataSetChanged();
                loadLastMessage(tutorResponseList);
                break;
            case AppConstants.TASKCODES.UPDATE_TOKEN:
                FcmTokenUpdateResponse fcmTokenUpdateResponse = (FcmTokenUpdateResponse) response;
                if (fcmTokenUpdateResponse != null) {
                    if (fcmTokenUpdateResponse.isStatus()) {
                        Preferences.saveData(AppConstants.PREF_KEYS.IS_TOKEN_UPDATE, false);
                    }
                }
                break;
            case AppConstants.TASKCODES.GET_GROUP_RESPONSE:
                GroupResponse groupResponse = (GroupResponse) response;
                if (groupResponse != null) {
                    List<GroupData> data = groupResponse.getData();
                    homeList.addAll(data);

                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        BaseApi baseApi = homeList.get(position);
        if (baseApi instanceof TutorResponse) {
            TutorResponse tutorResponse = (TutorResponse) baseApi;
            holder.tv_title.setText(tutorResponse.getName());
            holder.tv_description.setText(tutorResponse.getTagline());
            holder.tv_date.setText(tutorResponse.getFormatedCreatedDate());
            String image = tutorResponse.getImage();
            if (!TextUtils.isEmpty(image)) {
                Picasso.with(this).load(Util.getCompleteUrl(image)).placeholder(R.mipmap.accountselected2x).into(holder.imageView);
            } else {
                holder.imageView.setImageResource(R.mipmap.accountselected2x);
            }
            ChatMessage chatMessage = lastMessageMap.get(tutorResponse.getId());
            if (chatMessage != null) {
                holder.tv_date.setText(chatMessage.getTimeString());
                String messageType = chatMessage.getMessageType();
                switch (messageType) {
                    case AppConstants.META_TYPES.TEXT:
                        ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
                        if (chatText != null) {
                            holder.tv_description.setText(chatText.getText());
                        }
                        break;
                    case AppConstants.META_TYPES.FILE:
                        holder.tv_description.setText("Image");
                        break;
                }
            } else {
                holder.tv_description.setText("");
                holder.tv_date.setText("");
            }
        }
        if (baseApi instanceof GroupData) {
            GroupData tutorResponse = (GroupData) baseApi;
            holder.tv_title.setText(tutorResponse.getName());
            holder.tv_description.setText("");
            holder.tv_date.setText(tutorResponse.getFormatedCreatedDate());
            holder.imageView.setImageResource(R.mipmap.accountselected2x);
            ChatMessage chatMessage = lastMessageMap.get(tutorResponse.getId());
            if (chatMessage != null) {
                holder.tv_date.setText(chatMessage.getTimeString());
                String messageType = chatMessage.getMessageType();
                switch (messageType) {
                    case AppConstants.META_TYPES.TEXT:
                        ChatText chatText = MetaInfo.getChatText(chatMessage.getMetaInfoString());
                        if (chatText != null) {
                            holder.tv_description.setText(chatText.getText());
                        }
                        break;
                    case AppConstants.META_TYPES.FILE:
                        holder.tv_description.setText("Image");
                        break;
                }
            } else {
                holder.tv_description.setText("");
                holder.tv_date.setText("");
            }
        }

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        BaseApi baseApi = homeList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, baseApi);
        startNextActivity(bundle, IndividualChatActivity.class);
    }

    class Holder {
        TextView tv_title, tv_description, tv_date;
        ImageView imageView;

        public Holder(View view) {
            tv_title = (TextView) view.findViewById(R.id.tv_name);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            imageView = (ImageView) view.findViewById(R.id.iv_profilePic);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                startNextActivity(ProfileDetails.class);
                break;
            case R.id.action_logout:
                Preferences.saveData(AppConstants.PREF_KEYS.IS_TOKEN_UPDATE, true);
                Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAndRemoveTask();
                } else {

                    finish();
                }
                break;
            case R.id.action_ask_query:
                startNextActivity(AskQueryActivity.class);
                break;
            case R.id.notification:
                startNextActivity(NotificationActivity.class);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onHomePressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        SocketService.startService(this);
    }

    private void loadLastMessage(final List<TutorResponse> arrayList) {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                for (TutorResponse studentBaseClass : arrayList) {
                    List<ChatMessage> chatMessages = new Select().from(ChatMessage.class).where("toUserID=? or fromUserID=? LIMIT 1", studentBaseClass.getId(), studentBaseClass.getId()).execute();
                    if (chatMessages != null && chatMessages.size() > 0) {
                        ChatMessage chatMessage = chatMessages.get(0);
                        lastMessageMap.put(studentBaseClass.getId(), chatMessage);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                customListAdapter.notifyDataSetChanged();
            }
        }.execute();
    }

    @Override
    protected int getHomeIcon() {
        return R.mipmap.ic_home_header;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


}

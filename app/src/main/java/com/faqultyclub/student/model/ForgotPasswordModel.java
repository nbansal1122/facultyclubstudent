package student.faqulty.club.model;

import simplifii.framework.activity.BaseActivity;

/**
 * Created by saurabh on 20-09-2016.
 */
public class ForgotPasswordModel extends BaseApi{
    String msgPass, token;

    public String getMsg() {
        return msgPass;
    }

    public void setPassMsg(String msgPass) {
        this.msgPass = msgPass;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

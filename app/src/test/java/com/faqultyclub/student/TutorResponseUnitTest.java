package com.faqultyclub.student;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TutorResponse local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TutorResponseUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}
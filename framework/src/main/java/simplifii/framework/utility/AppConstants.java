package simplifii.framework.utility;

import java.util.LinkedHashMap;

public interface AppConstants {
    String DEF_REGULAR_FONT = "OpenSans-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer, String> storeCategory = new LinkedHashMap<Integer, String>();
    String REGISTRATION_COMPLETE = "registrationComplete";
    int REQUESTCODE_GOOGLE_SIGHN_IN = 101;
    String USER_TYPE_STUDENT = "student";

    interface ASSETS_RESOURCES {
        String JSON_FOLDER = "jsons";
        String TUTOR_PROFILE_STRUCTURE = JSON_FOLDER + "/tutor_profile_structure.json";
    }

    interface REQUEST_CODES {

        int GOOGLE_SIGHN_IN = 10;
        int REGISTER = 11;
        int UPDATE_PROFILE = 12;
    }

    interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }

    interface MEDIA_TYPES {
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
    }

    interface USER_TYPES {
        String STUDENT = "STUDENT";
        String TUTOR = "TUTOR";
        String TCENTRE = "TCENTRE";
    }

    interface META_TYPES {
        String TEXT = "TEXT";
        String FEED = "FEED";
        String FILE = "FILE";
    }

    interface ERROR_CODES {

        final int UNKNOWN_ERROR = 0;
        final int NO_INTERNET_ERROR = 1;
        final int NETWORK_SLOW_ERROR = 2;
        final int URL_INVALID = 3;
        final int DEVELOPMENT_ERROR = 4;

    }

    interface PAGE_URL {
        //        String PHOTO_URL = "http://ec2-54-201-39-154.us-west-2.compute.amazonaws.com:4000";
        String PHOTO_URL = "https://faqulty.club/mobile";
        String BASEURL = PHOTO_URL + "/";
        String SIGNUP = BASEURL + "signup";
        String OTP_VERIFY = BASEURL + "verify-phone";
        String LOGIN = BASEURL + "login";
        String GET_CITIES = BASEURL + "api/cities";
        String GET_LOCALITIES = BASEURL + "api/localities";
        String GET_USER = BASEURL + "api/users/";
        String UPDATE_USER = BASEURL + "api/users/";
        String UPDATE_TUTION_DETAILS = BASEURL + "api/users/";
        String GET_SUBJECTS = BASEURL + "api/subjects";
        String GET_DEGREE_SUBJECTS = BASEURL + "api/degree-subjects";
        String GET_CLASSES = BASEURL + "api/classes";
        String GET_BOARDS = BASEURL + "api/boards";
        String GET_MODES = BASEURL + "api/modes";
        String GET_LANGUAGES = BASEURL + "api/languages";
        String UPDATE_SOCIAL_DETAILS = BASEURL + "api/";
        String GET_DEGREE = BASEURL + "api/degrees";
        String GET_INSTITUTES = BASEURL + "api/institutes";
        String FORGOT_PASSWORD = BASEURL + "forgot-password";
        String RESET_PASSWORD = BASEURL + "reset-password";
        String UPLOAD_IMAGE = BASEURL + "file-ingest";
        String GET_IMAGE = BASEURL;
        String GET_SCHOOLS = BASEURL + "api/schools";
        String SOCIAL_LOGIN = BASEURL + "pre-login";
        String GET_TUTORS = BASEURL + "api/students/%s/tutors";
        String GET_GROUP = BASEURL + "api/students/%s/groups";
        String CHAT_URL = BASEURL;
        String FCM_UPDATE_TOKEN = BASEURL + "api/push-token";
        String ASK_QUERY = BASEURL + "api/contact";
        String GET_NOTIFICATIONS = BASEURL + "api/tutors/%s/notifications";
//        String MOBILEVERIFY = BASEURL + "/api/users/" + Preferences.getData(PREF_KEYS.USER_ID);
    }

    interface PREF_KEYS {

        String KEY_LOGIN = "IsUserLoggedIn";
        String KEY_USERNAME = "username";
        String KEY_EMAIL = "email";
        String KEY_PASSWORD = "password";
        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";
        String USER_TOKEN = "user_token";
        String IS_LOGIN = "is_login";
        String IS_FIRST = "is_first";
        String PHONE_NO = "phoneno";
        String SOURCE = "source";
        String USER_TYPE = "userType";
        String USER_ID = "userId";
        String FCM_TOKEN = "fcm_token";
        String IS_TOKEN_UPDATE = "is_fcm_token_update";
        String IS_PROFILE_COMPLETE = "is_profile_complete";
    }

    interface BUNDLE_KEYS {
        final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String SOCIALSIGNUP = "isSocialSignUp";
        String FRAGMENT_MESSAGE = "fragment_message";
        String POST_FEED = "post_feed";
        String KEY_URL = "key_url";
        String MIME_TYPE = "mime_type";
        String PDF_URL = "pdf_url";
    }


    interface VIEW_TYPE {
        int CARD_MY_TEAM = 0;
        int GET_IMAGE = 1;
        int GET_AUDIO = 2;
        int GET_PDF = 3;
        int GET_VIDEO = 4;
        int GET_HEADER = 5;

        interface CHAT_TYPES {
            int TEXT_MESSAGE_RECEIVE = 100;
            int TEXT_MESSAGE_SEND = 101;
            int MEDIA_MESSAGE_SEND = 102;
            int MEDIA_MESSAGE_RECEIVE = 103;
            int BOOKMARK_MESSAGE_SEND = 104;
            int BOOKMARK_MESSAGE_RECEIVE = 105;
            int ASSIGNMENT_MESSAGE_SEND = 107;
            int ASSIGNMENT_MESSAGE_RECEIVE = 108;
        }
    }


    public interface TASKCODES {
        int LOGIN = 20;
        int REGISTER = 21;
        int FBREGISTER = 22;
        int FBLOGIN = 23;
        int SOCAIL_LOGIN = 24;
        int OFFER = 31;
        int SIGNUP = 32;
        int OTP = 33;

        int UPDATE_USER = 37;
        int UPDATE_TUTION_DETAILS = 38;

        int GET_CITIES = 34;
        int GET_LOCALITIES = 35;
        int GET_USER = 36;
        int GET_SUBJECTS = 39;
        int GET_CLASSES = 40;
        int GET_LANGUAGES = 41;
        int GET_MODES = 42;
        int GET_BOARDS = 40;
        int GET_INSTITUTES = 44;
        int GET_DEGREE_SUBJECTS = 45;
        int GET_DEGREE = 46;

        int RESEND_OTP = 43;
        int FORGOT_PASSWORD = 60;
        int RESET_PASSWORD = 61;
        int VERIFY_PHONE = 62;

        int EDIT_SOCIAL_PROFILE = 50;
        int EDIT_INTRO = 51;
        int EDIT_QUALIFICATION = 52;
        int EDIT_TUTION_DETAILS = 53;

        int FILE_UPLOAD = 70;
        int UPLOAD_IMAGE = 71;
        int GET_SCHOOLS = 47;
        int GET_TUTOR_RESPONSE = 48;
        int GET_GROUP_RESPONSE = 49;
        int UPDATE_TOKEN = 50;
        int ASK_QUERY = 51;
        int GET_NOTIFICATION = 52;
    }

    interface ProfileStructureType {
        int PERSONAL_DETAILS = 1;
        int LOCATION = 2;
        int TUTION_DETAILS = 3;
        int QUALIFICATION = 4;
        int SOCIAL_PROFILES = 5;
        int CONTENT = 6;
        int REVIEWS = 7;
    }

    public interface IMAGE_CODE {
        int IMAGE = 21;
    }

    public interface FILE_TYPES {

        String IMAGE = "image";
        String AUDIO = "audio";
        String VIDEO = "video";
        String PDF = "pdf";
    }

    public interface FILE_REQUEST_CODE {
        int IMAGE = 13;
        int AUDIO = 14;
        int VIDEO = 15;
        int PDF = 16;
    }

    public interface PAGE_TITLES {
        String SIGN_UP = "Sign Up";
        String FORGOT_PASSWORD = "Forgot Password";
        String LOGIN = "Login";
        String COMPLETE_PROFILE = "Complete Your Profile";
        String ENTER_OTP = "Enter OTP";
        String RESET_PASSWORD = "Reset Password";
        String APP_NAME = "Faqulty Club";
        String CHANGE_PASSWORD = "Change Password";
        String EDIT_PROFILE = "Edit Profile";
    }
}
